# -*- coding: utf-8 -*-


import time
import collections


def hamming_distance(dataset1, dataset2):

    distance = 0
    i = 0

    datasets_length = len(dataset1)

    while i < datasets_length:

        nucleotide1 = dataset1[i:i+1]
        nucleotide2 = dataset2[i:i+1]

        if nucleotide1 != nucleotide2:
            distance += 1

        i += 1

    return distance


def find_consensus(motifs, k):

    consensus = ''

    for i in range(0, k):

        options = []

        for motif in motifs:

            options.append(motif[i:i+1])

        counter = collections.Counter(options)

        consensus += counter.most_common()[0][0]

    return consensus


def calculate_score(motifs, k):

    score = 0

    consensus = find_consensus(motifs, k)

    for motif in motifs:

        score += hamming_distance(consensus, motif)

    return score


def calculate_probability(pattern, profile):

    charset = ['A', 'C', 'G', 'T']

    probability = 1

    for index, nucleotide in enumerate(pattern):

        profile_i = charset.index(nucleotide)
        profile_j = index

        probability *= profile[profile_i][profile_j]

    return probability


def profile_most_probable(string, k, profile):

    most_probable_pattern = ''
    maximum_probability = -1

    i = 0

    string_length = len(string)

    while i <= string_length - k:

        pattern = string[i:i+k]
        pattern_probability = calculate_probability(pattern, profile)

        if pattern_probability > maximum_probability:

            most_probable_pattern = pattern
            maximum_probability = pattern_probability

        i += 1

    return most_probable_pattern


def calculate_profile_matrix(motifs, k):

    charset = ['A', 'C', 'G', 'T']

    motifs_length = len(motifs)

    profile_matrix = [[0.0 for row in range(0, k)] for col in range(0, len(charset))]

    for motif in motifs:

        for profile_i, nucleotide in enumerate(charset):

            for profile_j in range(0, k):

                if motif[profile_j] == nucleotide:

                    profile_matrix[profile_i][profile_j] += 1.0 / motifs_length

    return profile_matrix


def greedy_motif_search(dna, k):

    best_motifs = []

    for strand in dna:

        best_motifs.append(strand[:k])

    base_strand = dna[0]
    other_strands = dna[1:]

    strand_length = len(base_strand)

    i = 0

    while i <= strand_length - k:

        motifs = [base_strand[i:i+k]]

        for strand in other_strands:

            profile_matrix = calculate_profile_matrix(motifs, k)

            generated_motif = profile_most_probable(strand, k, profile_matrix)

            motifs.append(generated_motif)

        i += 1

        if calculate_score(motifs, k) < calculate_score(best_motifs, k):

            best_motifs = motifs

    return best_motifs


f = open('dataset_159_5.txt', 'r')

example = f.read().splitlines()

start = time.time()

# print 'Best motifs:'
# print '\n' . join(greedy_motif_search(['GGCGTTCAGGCA', 'AAGAATCAGTCA', 'CAAGGAGTTCGC', 'CACGTCAATCAC', 'CAATAATATTCG'], 3))

print 'Best motifs:'
print '\n' . join(greedy_motif_search(example, 12))

end = time.time()

print 'Execution time: ' + str(end - start) + ' segs'

f.close()