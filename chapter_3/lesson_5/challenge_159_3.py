# -*- coding: utf-8 -*-


import time


def calculate_probability(pattern, profile):

    charset = ['A', 'C', 'G', 'T']

    probability = 1

    for index, nucleotide in enumerate(pattern):

        profile_i = charset.index(nucleotide)
        profile_j = index

        probability *= profile[profile_i][profile_j]

    return probability


def profile_most_probable(string, k, profile):

    most_probable_pattern = ''
    maximum_probability = 0

    i = 0

    string_length = len(string)

    while i <= string_length - k:

        pattern = string[i:i+k]
        pattern_probability = calculate_probability(pattern, profile)

        if pattern_probability > maximum_probability:

            most_probable_pattern = pattern
            maximum_probability = pattern_probability

            print most_probable_pattern + ' ---> ' + str(maximum_probability)

        i += 1

    return most_probable_pattern


f = open('dataset_159_3.txt', 'r')

lines = f.read().splitlines()

dataset = lines[0]
k = int(lines[1])
profile = [map(float, lines[2].split()), map(float, lines[3].split()), map(float, lines[4].split()), map(float, lines[5].split())]

start = time.time()

# print 'Pattern: ' + profile_most_probable('ACCTGTTTATTGCCTAAGTTCCGAACAAACCCAATATAGCCCGAGGGCCT', 5, [[0.2, 0.2, 0.3, 0.2, 0.3], [0.4, 0.3, 0.1, 0.5, 0.1], [0.3, 0.3, 0.5, 0.2, 0.4], [0.1, 0.2, 0.1, 0.1, 0.2]])

print 'Pattern: ' + profile_most_probable(dataset, k, profile) + ' (expected output: TGTCGC)'

end = time.time()

print 'Execution time: ' + str(end - start) + ' segs'

f.close()