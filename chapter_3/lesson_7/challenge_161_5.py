# -*- coding: utf-8 -*-


import time
import collections
import random


def hamming_distance(dataset1, dataset2):

    distance = 0
    i = 0

    datasets_length = len(dataset1)

    while i < datasets_length:

        nucleotide1 = dataset1[i:i+1]
        nucleotide2 = dataset2[i:i+1]

        if nucleotide1 != nucleotide2:
            distance += 1

        i += 1

    return distance


def find_consensus(motifs, k):

    consensus = ''

    for i in range(0, k):

        options = []

        for motif in motifs:

            options.append(motif[i:i+1])

        counter = collections.Counter(options)

        consensus += counter.most_common()[0][0]

    return consensus


def calculate_score(motifs, k):

    score = 0

    consensus = find_consensus(motifs, k)

    for motif in motifs:

        score += hamming_distance(consensus, motif)

    return score


def calculate_probability(pattern, profile):

    charset = ['A', 'C', 'G', 'T']

    probability = 1

    for index, nucleotide in enumerate(pattern):

        profile_i = charset.index(nucleotide)
        profile_j = index

        probability *= profile[profile_i][profile_j]

    return probability


def profile_most_probable(string, k, profile):

    most_probable_pattern = ''
    maximum_probability = -1

    i = 0

    string_length = len(string)

    while i <= string_length - k:

        pattern = string[i:i+k]
        pattern_probability = calculate_probability(pattern, profile)

        if pattern_probability > maximum_probability:

            most_probable_pattern = pattern
            maximum_probability = pattern_probability

        i += 1

    return most_probable_pattern


def calculate_profile_matrix_with_pseudocounts(motifs, k):

    charset = ['A', 'C', 'G', 'T']

    motifs_length = len(motifs)

    profile_matrix = [[1.0 / k for row in range(0, k)] for col in range(0, len(charset))]

    for motif in motifs:

        for profile_i, nucleotide in enumerate(charset):

            for profile_j in range(0, k):

                if motif[profile_j] == nucleotide:

                    profile_matrix[profile_i][profile_j] += 1.0 / (motifs_length + k)

    return profile_matrix


def randomized_motif_search(dna, k):

    motifs = []

    strand_length = len(dna[0])

    for strand in dna:
        seed = random.randrange(0, strand_length - k + 1)
        motifs.append(strand[seed:seed+k])

    best_motifs = list(motifs)

    while True:

        profile_matrix = calculate_profile_matrix_with_pseudocounts(motifs, k)

        motifs = []

        for strand in dna:

            generated_motif = profile_most_probable(strand, k, profile_matrix)
            motifs.append(generated_motif)

        if calculate_score(motifs, k) < calculate_score(best_motifs, k):

            best_motifs = list(motifs)

        else:

            return best_motifs


def randomized_motif_search_iterations(dna, k, iterations):

    best_motifs = []
    best_score = 987654321

    for i in range(0, iterations):

        motifs = randomized_motif_search(dna, k)
        score = calculate_score(motifs, k)

        if score < best_score:

            best_motifs = list(motifs)
            best_score = score

            print 'Iteration (' + str(i) + ') (score: ' + str(best_score) + ')'

    return best_motifs


f = open('dataset_161_5.txt', 'r')

example = f.read().splitlines()

start = time.time()

# print str(calculate_score(['TCTCGGGG', 'CCAAGGTG', 'TACAGGCG', 'TTCAGGTG', 'TCCACGTG'], 8))

# print 'Randomized motifs with pseudo-count: (1000 iterations)'
# print '\n' . join(randomized_motif_search_iterations(['CGCCCCTCTCGGGGGTGTTCAGTAAACGGCCA', 'GGGCGAGGTATGTGTAAGTGCCAAGGTGCCAG', 'TAGTACCGAGACCGAAAGAAGTATACAGGCGT', 'TAGATCAAGTTTCAGGTGCACGTCGGTGAACC', 'AATCCACCAGCTCCACGTGCAATGTTGGCCTA'], 8, 1000))

print 'Randomized motifs with pseudo-count: (500 iterations)'
print '\n' . join(randomized_motif_search_iterations(example, 15, 500))

end = time.time()

print 'Execution time: ' + str(end - start) + ' segs'

f.close()