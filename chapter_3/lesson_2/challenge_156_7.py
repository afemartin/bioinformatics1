# -*- coding: utf-8 -*-


import itertools
import time


def generate_approximate_patterns(pattern, min_distance):

    charset = 'ACGT'

    for indices in itertools.combinations(range(len(pattern)), min_distance):
        for replacements in itertools.product(charset, repeat=min_distance):
            mutation = list(pattern)
            for index, replacement in zip(indices, replacements):
                mutation[index] = replacement
            yield '' . join(mutation)


def hamming_distance(dataset1, dataset2):

    distance = 0
    i = 0

    datasets_length = len(dataset1)

    while i < datasets_length:

        nucleotide1 = dataset1[i:i+1]
        nucleotide2 = dataset2[i:i+1]

        if nucleotide1 != nucleotide2:
            distance += 1

        i += 1

    return distance


def motif_enumeration(dna, k, d):

    candidates_samples = []
    generated_patterns = []

    for string in dna:

        i = 0

        string_length = len(string)

        while i <= string_length - k:
            candidates_samples.append(string[i:i+k])
            i += 1

    # remove repeated candidates
    candidates_samples = list(set(candidates_samples))

    for candidate in candidates_samples:
        generated_patterns += generate_approximate_patterns(candidate, d)

    # remove repeated patterns
    generated_patterns = list(set(generated_patterns))

    motifs = []

    for pattern in generated_patterns:

        motif_found_in_dna = True

        for string in dna:

            motif_found_in_string = False

            i = 0

            string_length = len(string)

            while i <= string_length - k:

                if hamming_distance(pattern, string[i:i+k]) <= d:
                    motif_found_in_string = True
                    break

                i += 1

            if not motif_found_in_string:
                motif_found_in_dna = False
                break

        if motif_found_in_dna:
            motifs.append(pattern)

    return sorted(motifs)


f = open('dataset_156_7.txt', 'r')

example = f.read().splitlines()

start = time.time()

# print 'Motifs: ' + ' ' . join(motif_enumeration(['ATTTGGC', 'TGCCTTA', 'CGGTATC', 'GAAAATT'], 3, 1))

# print 'Motifs: ' + ' ' . join(motif_enumeration(['TCTGAGCTTGCGTTATTTTTAGACC', 'GTTTGACGGGAACCCGACGCCTATA', 'TTTTAGATTTCCTCAGTCCACTATA', 'CTTACAATTTCGTTATTTATCTAAT', 'CAGTAGGAATAGCCACTTTGTTGTA', 'AAATCCATTAAGGAAAGACGACCGT'], 5, 2))

print 'Motifs: ' + ' ' . join(motif_enumeration(example, 5, 1))

end = time.time()

print 'Execution time: ' + str(end - start) + ' segs'

f.close()