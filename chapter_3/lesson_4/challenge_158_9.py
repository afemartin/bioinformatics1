# -*- coding: utf-8 -*-


import itertools
import time


def generate_patterns(length):

    charset = 'ACGT'

    return ['' . join(c) for c in itertools.product(charset, repeat=length)]


def hamming_distance(dataset1, dataset2):

    distance = 0
    i = 0

    datasets_length = len(dataset1)

    while i < datasets_length:

        nucleotide1 = dataset1[i:i+1]
        nucleotide2 = dataset2[i:i+1]

        if nucleotide1 != nucleotide2:
            distance += 1

        i += 1

    return distance


def calculate_distance(dna, pattern):

    k = len(pattern)

    distance = 0

    for string in dna:

        minimun_distance = k

        i = 0

        string_length = len(string)

        while i <= string_length - k:

            current_distance = hamming_distance(pattern, string[i:i+k])

            if current_distance < minimun_distance:
                minimun_distance = current_distance

            i += 1

        distance += minimun_distance

    return distance


def median_string(dna, k):

    generated_patterns = generate_patterns(k)

    minimun_distance = 9876543210

    median = ''

    for pattern in generated_patterns:

        current_distance = calculate_distance(dna, pattern)

        if current_distance <= minimun_distance:

            minimun_distance = current_distance
            median = pattern

    return median


f = open('dataset_158_9.txt', 'r')

example = f.read().splitlines()

start = time.time()

# print 'Pattern: ' + median_string(['AAATTGACGCAT', 'GACGACCACGTT', 'CGTCAGCGCCTG', 'GCTGAGCACCGG', 'AGTACGGGACAG'], 3)

print 'Pattern: ' + median_string(example, 6)

end = time.time()

print 'Execution time: ' + str(end - start) + ' segs'

f.close()