# -*- coding: utf-8 -*-


import time
import collections
import random


def hamming_distance(dataset1, dataset2):

    distance = 0
    i = 0

    datasets_length = len(dataset1)

    while i < datasets_length:

        nucleotide1 = dataset1[i:i+1]
        nucleotide2 = dataset2[i:i+1]

        if nucleotide1 != nucleotide2:
            distance += 1

        i += 1

    return distance


def find_consensus(motifs, k):

    consensus = ''

    for i in range(0, k):

        options = []

        for motif in motifs:

            options.append(motif[i:i+1])

        counter = collections.Counter(options)

        consensus += counter.most_common()[0][0]

    return consensus


def calculate_score(motifs, k):

    score = 0

    consensus = find_consensus(motifs, k)

    for motif in motifs:

        score += hamming_distance(consensus, motif)

    return score


def calculate_probability(pattern, profile):

    charset = ['A', 'C', 'G', 'T']

    probability = 1

    for index, nucleotide in enumerate(pattern):

        profile_i = charset.index(nucleotide)
        profile_j = index

        probability *= profile[profile_i][profile_j]

    return probability


def profile_most_probable(string, k, profile):

    most_probable_pattern = ''
    maximum_probability = -1

    i = 0

    string_length = len(string)

    while i <= string_length - k:

        pattern = string[i:i+k]
        pattern_probability = calculate_probability(pattern, profile)

        if pattern_probability > maximum_probability:

            most_probable_pattern = pattern
            maximum_probability = pattern_probability

        i += 1

    return most_probable_pattern


def calculate_profile_matrix_with_pseudocounts(motifs, k):

    charset = ['A', 'C', 'G', 'T']

    motifs_length = len(motifs)

    profile_matrix = [[1.0 / k for row in range(0, k)] for col in range(0, len(charset))]

    for motif in motifs:

        for profile_i, nucleotide in enumerate(charset):

            for profile_j in range(0, k):

                if motif[profile_j] == nucleotide:

                    profile_matrix[profile_i][profile_j] += 1.0 / (motifs_length + k)

    return profile_matrix


def gibbs_sampler(dna, k, iterations):

    motifs = []

    dna_length = len(dna)
    strand_length = len(dna[0])

    for strand in dna:
        seed = random.randint(0, strand_length - k)
        motifs.append(strand[seed:seed+k])

    best_motifs = list(motifs)

    for i in range(0, iterations):

        row = random.randint(0, dna_length - 1)

        motifs_without_deleted_row = list(motifs)
        motifs_without_deleted_row.remove(motifs[row])

        profile_matrix = calculate_profile_matrix_with_pseudocounts(motifs_without_deleted_row, k)

        # print motifs
        motifs[row] = profile_most_probable(dna[row], k, profile_matrix)
        # print motifs

        score = calculate_score(motifs, k)
        best_score = calculate_score(best_motifs, k)

        # print str(score) + ' < ' + str(best_score) + ' - changed row ' + str(row + 1) + ' random selected motif for ' + motifs[row]
        # print '************************************************************************************'

        if score < best_score:

            best_motifs = list(motifs)

            # print 'Iteration (' + str(i) + ') (score: ' + str(score) + ')'

    return best_motifs


def gibbs_sampler_iterations(dna, k, iterations):

    best_motifs = []
    best_score = 987654321

    for i in range(0, 50):

        motifs = gibbs_sampler(dna, k, iterations)
        score = calculate_score(motifs, k)

        if score < best_score:

            best_motifs = list(motifs)
            best_score = score

            print 'Iteration (' + str(i) + ') (score: ' + str(best_score) + ')'

    return best_motifs


f = open('dataset_163_4.txt', 'r')

example = f.read().splitlines()

start = time.time()

# print 'Gibbs sampler motifs:'
# print '\n' . join(gibbs_sampler_iterations(['CGCCCCTCTCGGGGGTGTTCAGTAAACGGCCA', 'GGGCGAGGTATGTGTAAGTGCCAAGGTGCCAG', 'TAGTACCGAGACCGAAAGAAGTATACAGGCGT', 'TAGATCAAGTTTCAGGTGCACGTCGGTGAACC', 'AATCCACCAGCTCCACGTGCAATGTTGGCCTA'], 8, 100))

print 'Gibbs sampler motifs:'
print '\n' . join(gibbs_sampler_iterations(example, 15, 200))

end = time.time()

print 'Execution time: ' + str(end - start) + ' segs'

f.close()