# -*- coding: utf-8 -*-


import itertools
import random


def generate_patterns(pattern_length):

    patterns = []

    for i in itertools.product(['0', '1'], repeat=pattern_length):
        patterns.append(''.join(map(str, i)))

    return patterns


def calculate_de_bruijn_graph(patterns):

    k = len(patterns[0])

    adjacent_patterns = {}

    graph = []

    for pattern in patterns:

        adjacent_patterns[pattern[:k-1]] = list()

    for pattern in patterns:

        adjacent_patterns[pattern[:k-1]].append(pattern[1:])

    for node, adjacent in adjacent_patterns.iteritems():

        graph.append(node + ' -> ' + ',' . join(sorted(adjacent)))

    return sorted(list(set(graph)))


def find_cycle(euler_graph, node):

    cycle = []

    while (len(cycle) < 2 or node != cycle[0]) and len(euler_graph[node]) > 0:

        cycle.append(node)

        adjacent_node = random.choice(euler_graph[node])

        euler_graph[node].remove(adjacent_node)

        node = adjacent_node

    cycle.append(node)

    return cycle


def calculate_eulerian_cycle(euler_graph, edges_count):

    # EULERIANCYCLE(Graph)
    # form a cycle Cycle by randomly walking in Graph (don't visit the same edge twice!)
    # while there are unexplored edges in Graph
    #     select a node newStart in Cycle with still unexplored edges
    #     form Cycle’ by traversing Cycle (starting at newStart) and then randomly walking
    #     Cycle ← Cycle’
    # return Cycle

    nodes = euler_graph.keys()

    node = random.choice(nodes)

    cycle = find_cycle(euler_graph, node)

    edges_visited = len(cycle) - 1

    while edges_visited < edges_count:

        nodes_with_unexplored_edges = []

        for node in cycle:

            if len(euler_graph[node]) > 0:

                nodes_with_unexplored_edges.append(node)

        new_start_node = random.choice(nodes_with_unexplored_edges)

        start_node_index = cycle.index(new_start_node)

        cycle.pop()     # get rid of the last repeated node

        cycle2 = cycle + cycle

        cycle = cycle2[start_node_index:start_node_index + edges_visited + 1]

        cycle.pop()     # get rid of the last repeated node

        cycle += find_cycle(euler_graph, new_start_node)

        edges_visited = len(cycle) - 1

    return cycle


def calculate_universal_cycle(k):

    patterns = generate_patterns(k)

    adjacency_list = calculate_de_bruijn_graph(patterns)

    euler_graph = {}

    edges_count = 0

    for adjacent_node in adjacency_list:

        path = adjacent_node.split(' -> ')

        path_origin = path[0]
        path_ends = path[1].split(',')

        edges_count += len(path_ends)

        euler_graph[path_origin] = path_ends

    euler_cycle = calculate_eulerian_cycle(euler_graph, edges_count)

    euler_cycle.pop()     # get rid of the last repeated node

    universal_cycle = ''

    for node in euler_cycle:

        universal_cycle += node[k-2:]

    return universal_cycle


f = open('dataset_203_10.txt', 'r')

example = f.read()

# print 'Universal circular string:'
# print calculate_universal_cycle(4)
# print '0000110010111101 (expected result)'

print 'Universal circular string:'
print calculate_universal_cycle(int(example))

f.close()