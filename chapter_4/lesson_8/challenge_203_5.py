# -*- coding: utf-8 -*-


import random


def find_cycle(euler_graph, node):

    cycle = []

    while (len(cycle) < 2 or node != cycle[0]) and len(euler_graph[node]) > 0:

        cycle.append(node)

        adjacent_node = random.choice(euler_graph[node])

        euler_graph[node].remove(adjacent_node)

        node = adjacent_node

    cycle.append(node)

    return cycle


def calculate_eulerian_cycle(euler_graph, edges_count):

    # EULERIANCYCLE(Graph)
    # form a cycle Cycle by randomly walking in Graph (don't visit the same edge twice!)
    # while there are unexplored edges in Graph
    #     select a node newStart in Cycle with still unexplored edges
    #     form Cycle’ by traversing Cycle (starting at newStart) and then randomly walking
    #     Cycle ← Cycle’
    # return Cycle

    nodes = euler_graph.keys()

    node = random.choice(nodes)

    cycle = find_cycle(euler_graph, node)

    edges_visited = len(cycle) - 1

    while edges_visited < edges_count:

        nodes_with_unexplored_edges = []

        for node in cycle:

            if len(euler_graph[node]) > 0:

                nodes_with_unexplored_edges.append(node)

        new_start_node = random.choice(nodes_with_unexplored_edges)

        start_node_index = cycle.index(new_start_node)

        cycle.pop()     # get rid of the last repeated node

        cycle2 = cycle + cycle

        cycle = cycle2[start_node_index:start_node_index + edges_visited + 1]

        cycle.pop()     # get rid of the last repeated node

        cycle += find_cycle(euler_graph, new_start_node)

        edges_visited = len(cycle) - 1

    return cycle


def calculate_eulerian_path(adjacency_list):

    euler_graph = {}

    edges_count = 0

    for adjacent_node in adjacency_list:

        path = adjacent_node.split(' -> ')

        path_origin = int(path[0])
        path_ends = map(int, path[1].split(','))

        edges_count += len(path_ends)

        euler_graph[path_origin] = path_ends

    euler_graph_node_balanced = {}

    for node_origin, adjacent_nodes in euler_graph.iteritems():

        if node_origin in euler_graph_node_balanced:

            euler_graph_node_balanced[node_origin] -= len(adjacent_nodes)

        else:

            euler_graph_node_balanced[node_origin] = - len(adjacent_nodes)

        for node_end in adjacent_nodes:

            if node_end in euler_graph_node_balanced:

                euler_graph_node_balanced[node_end] += 1

            else:

                euler_graph_node_balanced[node_end] = 1

    for node, degree in euler_graph_node_balanced.iteritems():

        if degree == 1:

            missing_edge_from = node

        if degree == -1:

            missing_edge_to = node

    if missing_edge_from in euler_graph:

        euler_graph[missing_edge_from].append(missing_edge_to)

    else:

        euler_graph[missing_edge_from] = [missing_edge_to]

    euler_cycle = calculate_eulerian_cycle(euler_graph, edges_count)

    euler_cycle.pop()     # get rid of the last repeated node

    last_index = len(euler_cycle) - 1

    while euler_cycle[0] != missing_edge_to or euler_cycle[last_index] != missing_edge_from:

        euler_cycle.insert(0, euler_cycle.pop())

    return euler_cycle


f = open('dataset_203_5.txt', 'r')

example = f.read().splitlines()

# print 'Eulerian Path:'
# print '->' . join(map(str, calculate_eulerian_path(['0 -> 2', ' 1 -> 3', ' 2 -> 1', '3 -> 0,4', ' 6 -> 3,7', ' 7 -> 8', '8 -> 9', '9 -> 6'])))

print 'Eulerian Path:'
print '->' . join(map(str, calculate_eulerian_path(example)))

f.close()