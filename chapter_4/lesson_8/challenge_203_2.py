# -*- coding: utf-8 -*-


import random


# RANDOM AND USELESS SOLUTION

# def find_path(euler_graph, path, node):
# 
#     if len(euler_graph[node]) == 0:
# 
#         path.append(node)
# 
#     else:
# 
#         if len(euler_graph[node]) > 1:
# 
#             candidate_nodes = []
# 
#             for adjacent_node in euler_graph[node]:
# 
#                 if len(euler_graph[adjacent_node]) > 0:
# 
#                     candidate_nodes.append(adjacent_node)
# 
#             if len(candidate_nodes) > 0:
# 
#                 random_adjacent_node = random.choice(candidate_nodes)
# 
#             else:
# 
#                 random_adjacent_node = euler_graph[node][0]
# 
#         else:
# 
#             random_adjacent_node = euler_graph[node][0]
# 
#         euler_graph[node].remove(random_adjacent_node)
# 
#         path.append(node)
# 
#         find_path(euler_graph, path, random_adjacent_node)
# 
#     return path


# HIGH TIME COMSUMING SOLUTION

# def find_path(euler_graph, path, node, edges_count):
#
#     path.append(node)
#
#     if len(euler_graph[node]) == 0:
#
#         if len(path) < edges_count + 1:
#
#             return False
#
#         else:
#
#             return path
#
#     else:
#
#         for adjacent_node in euler_graph[node]:
#
#             euler_graph_copy = copy.deepcopy(euler_graph)
#
#             euler_graph_copy[node].remove(adjacent_node)
#
#             path_copy = copy.deepcopy(path)
#
#             eulerian_path = find_path(euler_graph_copy, path_copy, adjacent_node, edges_count)
#
#             if eulerian_path:
#
#                 break
#
#     return eulerian_path


def find_cycle(euler_graph, node):

    cycle = []

    while (len(cycle) < 2 or node != cycle[0]) and len(euler_graph[node]) > 0:

        cycle.append(node)

        adjacent_node = random.choice(euler_graph[node])

        euler_graph[node].remove(adjacent_node)

        node = adjacent_node

    cycle.append(node)

    return cycle


def calculate_eulerian_cycle_graph(adjacency_list):

    euler_graph = {}

    edges_count = 0

    for adjacency_node in adjacency_list:

        path = adjacency_node.split(' -> ')

        path_origin = int(path[0])
        path_ends = map(int, path[1].split(','))

        edges_count += len(path_ends)

        euler_graph[path_origin] = path_ends

    #############################################

    # EULERIANCYCLE(Graph)
    # form a cycle Cycle by randomly walking in Graph (don't visit the same edge twice!)
    # while there are unexplored edges in Graph
    #     select a node newStart in Cycle with still unexplored edges
    #     form Cycle’ by traversing Cycle (starting at newStart) and then randomly walking
    #     Cycle ← Cycle’
    # return Cycle

    nodes = euler_graph.keys()

    node = random.choice(nodes)

    cycle = find_cycle(euler_graph, node)

    print cycle

    edges_visited = len(cycle) - 1

    while edges_visited < edges_count:

        nodes_with_unexplored_edges = []

        for node in cycle:

            if len(euler_graph[node]) > 0:

                nodes_with_unexplored_edges.append(node)

        new_start_node = random.choice(nodes_with_unexplored_edges)

        start_node_index = cycle.index(new_start_node)

        cycle.pop()     # get rid of the last repeated node

        cycle2 = cycle + cycle

        cycle = cycle2[start_node_index:start_node_index + edges_visited + 1]

        cycle.pop()     # get rid of the last repeated node

        cycle += find_cycle(euler_graph, new_start_node)

        edges_visited = len(cycle) - 1

        print 'Found cycle of length: ' + str(edges_visited)

    return cycle


f = open('dataset_203_2.txt', 'r')

example = f.read().splitlines()

# print 'Eulerian Cycle:'
# print '->' . join(map(str, calculate_eulerian_cycle_graph(['0 -> 3', '1 -> 0', '2 -> 1,6', '3 -> 2', '4 -> 2', '5 -> 4', '6 -> 5,8', '7 -> 9', '8 -> 7', '9 -> 6'])))

print 'Eulerian Cycle:'
print '->' . join(map(str, calculate_eulerian_cycle_graph(example)))

f.close()