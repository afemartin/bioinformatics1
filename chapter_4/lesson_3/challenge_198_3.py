# -*- coding: utf-8 -*-


def string_spelled_by_a_genome_path(patterns):

    string = ''

    patterns_remaining = list(patterns)

    string = patterns_remaining.pop(0)

    k = len(string)

    i = 1

    while len(patterns_remaining) > 0:

        for pattern in patterns_remaining:

            if string[i:] == pattern[:k-1]:

                string += pattern[k-1]

                patterns_remaining.remove(pattern)

                i += 1

                break

    return string


f = open('dataset_198_3.txt', 'r')

example = f.read().splitlines()

# print 'String spelled by a genome path:'
# print string_spelled_by_a_genome_path(['ACCGA', 'CCGAA', 'CGAAG', 'GAAGC', 'AAGCT'])

print 'String spelled by a genome path:'
print string_spelled_by_a_genome_path(example)

f.close()