# -*- coding: utf-8 -*-


def calculate_adjacency_list(patterns):

    patterns = sorted(patterns)

    adjacent_nodes = []

    k = len(patterns[0])

    for pattern in patterns:

        adjacent_candidates = list(patterns)
        adjacent_candidates.remove(pattern)

        for pattern_candidate in adjacent_candidates:

            if pattern[1:] == pattern_candidate[:k-1]:

                adjacent_nodes.append(pattern + ' -> ' + pattern_candidate)

    return adjacent_nodes


f = open('dataset_198_9.txt', 'r')

example = f.read().splitlines()

# print 'Adjacency list:'
# print '\n' . join(calculate_adjacency_list(['ATGCG', 'GCATG', 'CATGC', 'AGGCA', 'GGCAT']))

print 'Adjacency list:'
print '\n' . join(calculate_adjacency_list(example))

f.close()