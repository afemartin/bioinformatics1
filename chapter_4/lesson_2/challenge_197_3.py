# -*- coding: utf-8 -*-


def composition(string, k):

    k_mers = []

    i = 0

    string_length = len(string)

    while i <= string_length - k:
        k_mers.append(string[i:i+k])
        i += 1

    return sorted(k_mers)


f = open('dataset_197_3.txt', 'r')

example = f.read()

# print 'Composition:'
# print '\n' . join(composition('CAATCCAAC', 5))

print 'Composition:'
print '\n' . join(composition(example, 100))

f.close()