# -*- coding: utf-8 -*-


def composition(string, k):

    k_mers = []

    i = 0

    string_length = len(string)

    while i <= string_length - k:
        k_mers.append(string[i:i+k])
        i += 1

    return sorted(k_mers)


def calculate_de_bruijn_graph(string, k):

    patterns = composition(string, k-1)

    graph = []

    for pattern in patterns:

        adjacent_patterns = []

        for pattern_candidate in patterns:

            if pattern[1:] == pattern_candidate[:k-2]:

                adjacent_patterns.append(pattern_candidate)

        adjacent_patterns = list(set(adjacent_patterns))

        if len(adjacent_patterns) > 0:

            graph.append(pattern + ' -> ' + ',' . join(adjacent_patterns))

    return sorted(list(set(graph)))


f = open('dataset_199_6.txt', 'r')

example = f.read()

# print 'DeBruijn Graph:'
# print '\n' . join(calculate_de_bruijn_graph('AAGATTCTCTAAGA', 4))

print 'DeBruijn Graph:'
print '\n' . join(calculate_de_bruijn_graph(example, 12))

f.close()