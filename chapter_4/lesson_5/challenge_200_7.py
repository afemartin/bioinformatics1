# -*- coding: utf-8 -*-


def calculate_de_bruijn_graph(patterns):

    k = len(patterns[0])

    adjacent_patterns = {}

    graph = []

    for pattern in patterns:

        adjacent_patterns[pattern[:k-1]] = list()

    for pattern in patterns:

        adjacent_patterns[pattern[:k-1]].append(pattern[1:])

    for node, adjacent in adjacent_patterns.iteritems():

        graph.append(node + ' -> ' + ',' . join(sorted(adjacent)))

    return sorted(list(set(graph)))


f = open('dataset_200_7.txt', 'r')

example = f.read().splitlines()

# print 'DeBruijn Graph:'
# print '\n' . join(calculate_de_bruijn_graph(['GAGG', 'CAGG', 'GGGG', 'GGGA', 'CAGG', 'AGGG', 'GGAG']))

print 'DeBruijn Graph:'
print '\n' . join(calculate_de_bruijn_graph(example))

f.close()