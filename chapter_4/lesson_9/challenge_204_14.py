# -*- coding: utf-8 -*-


import random


def paired_kmer_prefix(paired_kmer, k):

    paired_kmer = paired_kmer[0:k-1] + '|' + paired_kmer[k+1:k+k]

    return paired_kmer


def paired_kmer_suffix(paired_kmer, k):

    paired_kmer = paired_kmer[1:k] + '|' + paired_kmer[k+2:k+k+1]

    return paired_kmer


def calculate_de_bruijn_graph(paired_patterns, k):

    adjacent_paired_patterns = {}

    graph = []

    for paired_pattern in paired_patterns:

        index = paired_kmer_prefix(paired_pattern, k)

        adjacent_paired_patterns[index] = list()

    for paired_pattern in paired_patterns:

        index = paired_kmer_prefix(paired_pattern, k)

        value = paired_kmer_suffix(paired_pattern, k)

        adjacent_paired_patterns[index].append(value)

    for pair_node, pair_adjacent in adjacent_paired_patterns.iteritems():

        graph.append(pair_node + ' -> ' + ',' . join(sorted(pair_adjacent)))

    return sorted(list(set(graph)))


def find_cycle(euler_graph, node):

    cycle = []

    while (len(cycle) < 2 or node != cycle[0]) and len(euler_graph[node]) > 0:

        cycle.append(node)

        adjacent_node = random.choice(euler_graph[node])

        euler_graph[node].remove(adjacent_node)

        node = adjacent_node

    cycle.append(node)

    return cycle


def calculate_eulerian_path(euler_graph, edges_count):

    # EULERIANCYCLE(Graph)
    # form a cycle Cycle by randomly walking in Graph (don't visit the same edge twice!)
    # while there are unexplored edges in Graph
    #     select a node newStart in Cycle with still unexplored edges
    #     form Cycle’ by traversing Cycle (starting at newStart) and then randomly walking
    #     Cycle ← Cycle’
    # return Cycle

    nodes = euler_graph.keys()

    node = random.choice(nodes)

    cycle = find_cycle(euler_graph, node)

    edges_visited = len(cycle) - 1

    while edges_visited < edges_count:

        nodes_with_unexplored_edges = []

        for node in cycle:

            if len(euler_graph[node]) > 0:

                nodes_with_unexplored_edges.append(node)

        new_start_node = random.choice(nodes_with_unexplored_edges)

        start_node_index = cycle.index(new_start_node)

        cycle.pop()     # get rid of the last repeated node

        cycle2 = cycle + cycle

        cycle = cycle2[start_node_index:start_node_index + edges_visited + 1]

        cycle.pop()     # get rid of the last repeated node

        cycle += find_cycle(euler_graph, new_start_node)

        edges_visited = len(cycle) - 1

    return cycle


def calculate_genome_path(paired_kmers, k, d):

    adjacency_list = calculate_de_bruijn_graph(paired_kmers, k)

    # print adjacency_list

    euler_graph = {}

    edges_count = 0

    for adjacent_node in adjacency_list:

        path = adjacent_node.split(' -> ')

        path_origin = path[0]
        path_ends = path[1].split(',')

        edges_count += len(path_ends)

        euler_graph[path_origin] = path_ends

    # print euler_graph

    euler_graph_node_balanced = {}

    for node_origin, adjacent_nodes in euler_graph.iteritems():

        if node_origin in euler_graph_node_balanced:

            euler_graph_node_balanced[node_origin] -= len(adjacent_nodes)

        else:

            euler_graph_node_balanced[node_origin] = - len(adjacent_nodes)

        for node_end in adjacent_nodes:

            if node_end in euler_graph_node_balanced:

                euler_graph_node_balanced[node_end] += 1

            else:

                euler_graph_node_balanced[node_end] = 1

    # print euler_graph_node_balanced

    for node, degree in euler_graph_node_balanced.iteritems():

        if degree == 1:

            missing_edge_from = node

        if degree == -1:

            missing_edge_to = node

    if missing_edge_from in euler_graph:

        euler_graph[missing_edge_from].append(missing_edge_to)

    else:

        euler_graph[missing_edge_from] = [missing_edge_to]

    # print euler_graph
    # print edges_count

    euler_path = calculate_eulerian_path(euler_graph, edges_count)

    # print euler_path

    euler_path.pop()     # get rid of the last repeated node

    last_index = len(euler_path) - 1

    while euler_path[0] != missing_edge_to or euler_path[last_index] != missing_edge_from:

        euler_path.insert(0, euler_path.pop())

    # print euler_path

    genome_1 = euler_path[0][:k-2]

    for node in euler_path:

        genome_1 += node[k-2:k-1]

    # print genome_1

    genome_2 = euler_path[0][k:k+k-2]

    for node in euler_path:

        genome_2 += node[k+k-2:]

    # print genome_2

    return genome_1[:k+d] + genome_2


f = open('dataset_204_14.txt', 'r')

example = f.read().splitlines()

# print 'Genome Path:'
# print calculate_genome_path(['GAGA|TTGA', 'TCGT|GATG', 'CGTG|ATGT', 'TGGT|TGAG', 'GTGA|TGTT', 'GTGG|GTGA', 'TGAG|GTTG', 'GGTC|GAGA', 'GTCG|AGAT'], 4, 2)

# print 'Genome Path:'
# print calculate_genome_path(example, 50, 200)

print 'Genome Path: (quiz 4 - question 7)'

solutions = []

for i in range(0, 1000):
    solutions.append(calculate_genome_path(['ACC|ATA', 'ACT|ATT', 'ATA|TGA', 'ATT|TGA', 'CAC|GAT', 'CCG|TAC', 'CGA|ACT', 'CTG|AGC', 'CTG|TTC', 'GAA|CTT', 'GAT|CTG', 'GAT|CTG', 'TAC|GAT', 'TCT|AAG', 'TGA|GCT', 'TGA|TCT', 'TTC|GAA'], 3, 1))

# print '\n' .join(sorted(list(set(solutions))))

# print '######################################'

print sorted(list(set(solutions)))[1]
print sorted(list(set(solutions)))[3]

f.close()