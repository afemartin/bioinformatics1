# -*- coding: utf-8 -*-


# import copy


def calculate_de_bruijn_graph(patterns):

    k = len(patterns[0])

    adjacent_patterns = {}

    graph = []

    for pattern in patterns:

        adjacent_patterns[pattern[:k-1]] = list()

    for pattern in patterns:

        adjacent_patterns[pattern[:k-1]].append(pattern[1:])

    for node, adjacent in adjacent_patterns.iteritems():

        graph.append(node + ' -> ' + ',' . join(sorted(adjacent)))

    return sorted(list(set(graph)))


def calculate_euler_graph_balanced(euler_graph):

    euler_graph_node_balanced = {}

    for node_origin, adjacent_nodes in euler_graph.iteritems():

        if node_origin not in euler_graph_node_balanced:

            euler_graph_node_balanced[node_origin] = {'in': 0, 'out': 0}

        for node_end in adjacent_nodes:

            if node_end not in euler_graph_node_balanced:

                euler_graph_node_balanced[node_end] = {'in': 0, 'out': 0}

    for node_origin, adjacent_nodes in euler_graph.iteritems():

        euler_graph_node_balanced[node_origin]['out'] += len(adjacent_nodes)

        for node_end in adjacent_nodes:

            euler_graph_node_balanced[node_end]['in'] += 1

    return euler_graph_node_balanced


# def find_contig_paths(euler_graph, start_node):
#
#     print euler_graph
#
#     paths = []
#
#     node = start_node
#
#     path = start_node
#
#     while len(euler_graph[node]) > 0:
#
#         if len(euler_graph[node]) == 1:
#
#             next_node = euler_graph[node][0]
#
#             path += next_node[-1]
#
#             euler_graph[node].remove(next_node)
#
#             node = next_node
#
#             if node not in euler_graph:
#
#                 break
#
#         else:
#
#             euler_graph_iterable = copy.deepcopy(euler_graph)
#
#             for adjacent_node in euler_graph_iterable[node]:
#
#                 euler_graph[node] = [adjacent_node]
#
#                 paths += find_contig_paths(euler_graph, node)
#
#                 print paths
#
#     paths.append(path)
#
#     return sorted(paths)


# def calculate_contig_paths(patterns):
#
#     adjacency_list = calculate_de_bruijn_graph(patterns)
#
#     euler_graph = {}
#
#     edges_count = 0
#
#     for adjacent_node in adjacency_list:
#
#         path = adjacent_node.split(' -> ')
#
#         path_origin = path[0]
#         path_ends = path[1].split(',')
#
#         edges_count += len(path_ends)
#
#         euler_graph[path_origin] = path_ends
#
#     euler_graph_node_balanced = calculate_euler_graph_balanced(euler_graph)
#
#     start_node = ''
#
#     for node, degree in euler_graph_node_balanced.iteritems():
#
#         if degree['in'] == 0:
#
#             start_node = node
#
#             break
#
#     paths = find_contig_paths(euler_graph, start_node)
#
#     return paths


def maximal_non_branching_paths(patterns):

    adjacency_list = calculate_de_bruijn_graph(patterns)

    paths = []

    euler_graph = {}

    for adjacent_node in adjacency_list:

        path = adjacent_node.split(' -> ')

        path_origin = path[0]
        path_ends = path[1].split(',')

        euler_graph[path_origin] = path_ends

    euler_graph_node_balanced = calculate_euler_graph_balanced(euler_graph)

    for node in euler_graph:

        degre_in = euler_graph_node_balanced[node]['in']
        degre_out = euler_graph_node_balanced[node]['out']

        if (degre_in != 1 or degre_out != 1) and degre_out > 0:

            for adjacent_node in euler_graph[node]:

                path = node + adjacent_node[-1]

                degre_in = euler_graph_node_balanced[adjacent_node]['in']
                degre_out = euler_graph_node_balanced[adjacent_node]['out']

                while degre_in == 1 and degre_out == 1:

                    adjacent_node = euler_graph[adjacent_node][0]

                    path += adjacent_node[-1]

                    degre_in = euler_graph_node_balanced[adjacent_node]['in']
                    degre_out = euler_graph_node_balanced[adjacent_node]['out']

                paths.append(path)

    return sorted(paths)


f = open('dataset_205_5.txt', 'r')

example = f.read().splitlines()

# print 'Calculate contig paths:'
# print '\n' . join(maximal_non_branching_paths(['ATG', 'ATG', 'TGT', 'TGG', 'CAT', 'GGA', 'GAT', 'AGA']))

print 'Calculate contig paths:'
print '\n' . join(maximal_non_branching_paths(example))

f.close()