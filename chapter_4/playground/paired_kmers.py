# -*- coding: utf-8 -*-


def paired_kmer_prefix(paired_kmer):

    k = (len(paired_kmer) - 1) / 2

    paired_kmer = paired_kmer[0:k-1] + '|' + paired_kmer[k+1:k+k]

    return paired_kmer


def paired_kmer_suffix(paired_kmer):

    k = (len(paired_kmer) - 1) / 2

    paired_kmer = paired_kmer[1:k] + '|' + paired_kmer[k+2:k+k+1]

    return paired_kmer


def calculate_paired_compositions(string, k, d):

    paired_kmers = []

    string_lenth = len(string)

    i = 0

    while i <= string_lenth - k - k - d:

        pair = string[i:i+k] + '|' + string[i+k+d:i+k+d+k]

        paired_kmers.append(pair)

        i += 1

    return sorted(paired_kmers)


example = 'TAATGCCATGGGATGTT'

print '(' + ')(' . join(calculate_paired_compositions(example, 3, 2)) + ')'
print '(' + ')(' . join(map(paired_kmer_prefix, calculate_paired_compositions(example, 3, 2))) + ')'
print '(' + ')(' . join(map(paired_kmer_suffix, calculate_paired_compositions(example, 3, 2))) + ')'