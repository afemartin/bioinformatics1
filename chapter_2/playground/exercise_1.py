
aminoacid_dictionary = {'AAA': 'K', 'AAC': 'N', 'AAG': 'K', 'AAU': 'N', 'ACA': 'T', 'ACC': 'T', 'ACG': 'T', 'ACU': 'T', 'AGA': 'R', 'AGC': 'S', 'AGG': 'R', 'AGU': 'S', 'AUA': 'I', 'AUC': 'I', 'AUG': 'M', 'AUU': 'I', 'CAA': 'Q', 'CAC': 'H', 'CAG': 'Q', 'CAU': 'H', 'CCA': 'P', 'CCC': 'P', 'CCG': 'P', 'CCU': 'P', 'CGA': 'R', 'CGC': 'R', 'CGG': 'R', 'CGU': 'R', 'CUA': 'L', 'CUC': 'L', 'CUG': 'L', 'CUU': 'L', 'GAA': 'E', 'GAC': 'D', 'GAG': 'E', 'GAU': 'D', 'GCA': 'A', 'GCC': 'A', 'GCG': 'A', 'GCU': 'A', 'GGA': 'G', 'GGC': 'G', 'GGG': 'G', 'GGU': 'G', 'GUA': 'V', 'GUC': 'V', 'GUG': 'V', 'GUU': 'V', 'UAA': '', 'UAC': 'Y', 'UAG': '', 'UAU': 'Y', 'UCA': 'S', 'UCC': 'S', 'UCG': 'S', 'UCU': 'S', 'UGA': '', 'UGC': 'C', 'UGG': 'W', 'UGU': 'C', 'UUA': 'L', 'UUC': 'F', 'UUG': 'L', 'UUU': 'F'}


def dna_reverse_complement(dna):

    reverse_complement = ''

    for nucleotice in dna[::-1]:
        if nucleotice == 'A':
            reverse_complement += 'T'
        elif nucleotice == 'C':
            reverse_complement += 'G'
        elif nucleotice == 'G':
            reverse_complement += 'C'
        elif nucleotice == 'T':
            reverse_complement += 'A'

    return reverse_complement


def dna_to_rna(dna):

    return dna.replace('T', 'U')


def rna_to_aminoacid(rna):

    i = 0
    aminoacid_string = ''

    while i + 3 <= len(rna):
        codon = rna[i:i+3]
        aminoacid_string += aminoacid_dictionary.get(codon)
        i += 3

    return aminoacid_string


def find_aminoacid(dna, peptide):

    i = 0
    matches = []

    dna_length = len(dna)
    peptide_length = 3 * len(peptide)

    while i + peptide_length <= dna_length:

        dna_chunk_straight = dna[i:i+peptide_length]
        dna_chunk_reversed = dna_reverse_complement(dna_chunk_straight)

        codon_straight = dna_to_rna(dna_chunk_straight)
        codon_reversed = dna_to_rna(dna_chunk_reversed)

        if rna_to_aminoacid(codon_straight) == peptide or rna_to_aminoacid(codon_reversed) == peptide:
            matches.append(dna_chunk_straight)

        i += 1

    return matches


f = open('B_brevis.txt', 'r')

example = f.read()

print 'Peptides found: ' + ' ' . join(find_aminoacid(example, 'VKLFPWFNQY'))
print 'Peptides found: ' + ' ' . join(find_aminoacid(example, 'KLFPWFNQYV'))
print 'Peptides found: ' + ' ' . join(find_aminoacid(example, 'LFPWFNQYVK'))
print 'Peptides found: ' + ' ' . join(find_aminoacid(example, 'FPWFNQYVKL'))
print 'Peptides found: ' + ' ' . join(find_aminoacid(example, 'PWFNQYVKLF'))
print 'Peptides found: ' + ' ' . join(find_aminoacid(example, 'WFNQYVKLFP'))
print 'Peptides found: ' + ' ' . join(find_aminoacid(example, 'FNQYVKLFPW'))
print 'Peptides found: ' + ' ' . join(find_aminoacid(example, 'NQYVKLFPWF'))
print 'Peptides found: ' + ' ' . join(find_aminoacid(example, 'QYVKLFPWFN'))
print 'Peptides found: ' + ' ' . join(find_aminoacid(example, 'YVKLFPWFNQ'))

f.close