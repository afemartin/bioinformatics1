
molecular_masses = {'G': 57, 'A': 71, 'S': 87, 'P': 97, 'V': 99, 'T': 101, 'C': 103, 'I': 113, 'L': 113, 'N': 114, 'D': 115, 'K': 128, 'Q': 128, 'E': 129, 'M': 131, 'H': 137, 'F': 147, 'R': 156, 'Y': 163, 'W': 186}


def calculate_mass(aminoacid_string):

    mass = 0

    for aminoacid in aminoacid_string:
        mass += molecular_masses.get(aminoacid)

    return mass


print 'Peptide mass: ' + str(calculate_mass('VKLFPWFNQY'))
