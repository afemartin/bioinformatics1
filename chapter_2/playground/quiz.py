codons = {
    'G': 4,
    'A': 4,
    'S': 6,
    'P': 4,
    'V': 4,
    'T': 4,
    'C': 2,
    'I': 3,
    'L': 6,
    'N': 2,
    'D': 2,
    'K': 2,
    'Q': 2,
    'E': 2,
    'M': 1,
    'H': 2,
    'F': 2,
    'R': 6,
    'Y': 2,
    'W': 1
}

combinations = 1

for c in 'SYNGE':
    combinations *= codons.get(c)

print combinations