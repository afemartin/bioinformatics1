# -*- coding: utf-8 -*-


aminoacid_dictionary = {'AAA': 'K', 'AAC': 'N', 'AAG': 'K', 'AAU': 'N', 'ACA': 'T', 'ACC': 'T', 'ACG': 'T', 'ACU': 'T', 'AGA': 'R', 'AGC': 'S', 'AGG': 'R', 'AGU': 'S', 'AUA': 'I', 'AUC': 'I', 'AUG': 'M', 'AUU': 'I', 'CAA': 'Q', 'CAC': 'H', 'CAG': 'Q', 'CAU': 'H', 'CCA': 'P', 'CCC': 'P', 'CCG': 'P', 'CCU': 'P', 'CGA': 'R', 'CGC': 'R', 'CGG': 'R', 'CGU': 'R', 'CUA': 'L', 'CUC': 'L', 'CUG': 'L', 'CUU': 'L', 'GAA': 'E', 'GAC': 'D', 'GAG': 'E', 'GAU': 'D', 'GCA': 'A', 'GCC': 'A', 'GCG': 'A', 'GCU': 'A', 'GGA': 'G', 'GGC': 'G', 'GGG': 'G', 'GGU': 'G', 'GUA': 'V', 'GUC': 'V', 'GUG': 'V', 'GUU': 'V', 'UAA': '', 'UAC': 'Y', 'UAG': '', 'UAU': 'Y', 'UCA': 'S', 'UCC': 'S', 'UCG': 'S', 'UCU': 'S', 'UGA': '', 'UGC': 'C', 'UGG': 'W', 'UGU': 'C', 'UUA': 'L', 'UUC': 'F', 'UUG': 'L', 'UUU': 'F'}


def rna_to_aminoacid(rna):

    i = 0
    aminoacid_string = ''

    while i + 3 <= len(rna):
        codon = rna[i:i+3]
        aminoacid_string += aminoacid_dictionary.get(codon)
        i += 3

    return aminoacid_string


f = open('dataset_96_5.txt', 'r')

example = f.read()

# print rna_to_aminoacid('AUGGCCAUGGCGCCCAGAACUGAGAUCAAUAGUACCCGUAUUAACGGGUGA')

print rna_to_aminoacid(example)

# QUIZ 2 - QUESTION 2

print rna_to_aminoacid('CCCCGUACGGAGAUGAAA')
print rna_to_aminoacid('CCGAGGACCGAAAUCAAC')
print rna_to_aminoacid('CCCAGUACCGAAAUUAAC')
print rna_to_aminoacid('CCUCGUACAGAAAUCAAC')

f.close()