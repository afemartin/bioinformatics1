# -*- coding: utf-8 -*-


molecular_masses = {'G': 57, 'A': 71, 'S': 87, 'P': 97, 'V': 99, 'T': 101, 'C': 103, 'I': 113, 'L': 113, 'N': 114, 'D': 115, 'K': 128, 'Q': 128, 'E': 129, 'M': 131, 'H': 137, 'F': 147, 'R': 156, 'Y': 163, 'W': 186}


def calculate_mass(peptide):

    mass = 0

    for aminoacid in peptide:
        mass += molecular_masses.get(aminoacid)

    return mass


def generate_theoretical_spectrum(peptide):

    i = 0

    peptide_length = len(peptide)

    peptide_sample = peptide + peptide

    masses = []

    while i <= peptide_length:
        if i == 0:
            masses.append(calculate_mass(''))
        elif i < peptide_length:
            for j in range(0, peptide_length):
                masses.append(calculate_mass(peptide_sample[j:j+i]))
        else:
            masses.append(calculate_mass(peptide))
        i += 1

    return masses


f = open('dataset_98_4.txt', 'r')

example = f.read()

# print 'Cyclospectrum: ' + ' ' . join(str(n) for n in generate_theoretical_spectrum('LEQN'))

print 'Cyclospectrum: ' + ' ' . join(str(n) for n in generate_theoretical_spectrum(example))

# QUIZ 2 - QUESTION 5

print 'Cyclospectrum: ' + ' ' . join(str(n) for n in sorted(generate_theoretical_spectrum('TLAM')))
# print 'Cyclospectrum: ' + ' ' . join(str(n) for n in sorted(generate_theoretical_spectrum('TMIA')))
# print 'Cyclospectrum: ' + ' ' . join(str(n) for n in sorted(generate_theoretical_spectrum('TAIM')))
print 'Cyclospectrum: ' + ' ' . join(str(n) for n in sorted(generate_theoretical_spectrum('ALTM')))
# print 'Cyclospectrum: ' + ' ' . join(str(n) for n in sorted(generate_theoretical_spectrum('MIAT')))
print 'Cyclospectrum: ' + ' ' . join(str(n) for n in sorted(generate_theoretical_spectrum('MAIT')))

f.close()