# -*- coding: utf-8 -*-


molecular_masses = {'G': 57, 'A': 71, 'S': 87, 'P': 97, 'V': 99, 'T': 101, 'C': 103, 'I': 113, 'L': 113, 'N': 114, 'D': 115, 'K': 128, 'Q': 128, 'E': 129, 'M': 131, 'H': 137, 'F': 147, 'R': 156, 'Y': 163, 'W': 186}


def calculate_mass(peptide):

    mass = 0

    for aminoacid in peptide:
        mass += molecular_masses.get(aminoacid)

    return mass


def parent_mass(spectrum):

    return max(spectrum)


def expand(peptides):

    expanded = []

    for peptide in peptides:
        for aminoacid in ['G', 'A', 'S', 'P', 'V', 'T', 'C', 'I', 'N', 'D', 'K', 'E', 'M', 'H', 'F', 'R', 'Y', 'W']:
            expanded.append(peptide + aminoacid)

    return expanded


def is_equal(spectrum1, spectrum2):

    spectrum1 = '-' . join(str(n) for n in sorted(spectrum1))
    spectrum2 = '-' . join(str(n) for n in sorted(spectrum2))

    return spectrum1 == spectrum2


def is_consistent(spectrum1, spectrum2):

    result = True

    for aminoacid_mass in spectrum1:
        if aminoacid_mass not in spectrum2:
            result = False
            break

    return result


def generate_linear_spectrum(peptide):

    i = 0

    peptide_length = len(peptide)

    masses = []

    while i <= peptide_length:
        if i == 0:
            masses.append(calculate_mass(''))
        elif i < peptide_length:
            for j in range(0, peptide_length - i + 1):
                masses.append(calculate_mass(peptide[j:j+i]))
        else:
            masses.append(calculate_mass(peptide))
        i += 1

    return masses


def generate_cyclic_spectrum(peptide):

    i = 0

    peptide_length = len(peptide)

    peptide_sample = peptide + peptide

    masses = []

    while i <= peptide_length:
        if i == 0:
            masses.append(calculate_mass(''))
        elif i < peptide_length:
            for j in range(0, peptide_length):
                masses.append(calculate_mass(peptide_sample[j:j+i]))
        else:
            masses.append(calculate_mass(peptide))
        i += 1

    return masses


def cyclopeptide_sequencing(spectrum):

    spectrum = map(int, spectrum.split())

    peptides = ['G', 'A', 'S', 'P', 'V', 'T', 'C', 'I', 'N', 'D', 'K', 'E', 'M', 'H', 'F', 'R', 'Y', 'W']

    matches = []

    while len(peptides) > 0:

        peptides_iterable = list(peptides)

        for peptide in peptides_iterable:

            peptide_cyclic_spectrum = generate_cyclic_spectrum(peptide)
            peptide_linear_spectrum = generate_linear_spectrum(peptide)

            if calculate_mass(peptide) == parent_mass(spectrum):
                if is_equal(peptide_cyclic_spectrum, spectrum):
                    matches.append('-' . join(str(n) for n in map(calculate_mass, peptide)))
                peptides.remove(peptide)
            elif not is_consistent(peptide_linear_spectrum, spectrum):
                peptides.remove(peptide)

        peptides = expand(peptides)

    return matches


f = open('dataset_100_5.txt', 'r')

example = f.read()

# print 'Cyclopeptides: ' + ' ' . join(cyclopeptide_sequencing('0 113 128 186 241 299 314 427'))

# print 'Cyclopeptides: ' + ' ' . join(cyclopeptide_sequencing('0 71 97 99 103 113 113 114 115 131 137 196 200 202 208 214 226 227 228 240 245 299 311 311 316 327 337 339 340 341 358 408 414 424 429 436 440 442 453 455 471 507 527 537 539 542 551 554 556 566 586 622 638 640 651 653 657 664 669 679 685 735 752 753 754 756 766 777 782 782 794 848 853 865 866 867 879 885 891 893 897 956 962 978 979 980 980 990 994 996 1022 1093'))

print 'Cyclopeptides: ' + ' ' . join(cyclopeptide_sequencing(example))


# QUIZ 2 - QUESTION 8

print is_consistent(generate_linear_spectrum('CET'), map(int, str('0 71 99 101 103 128 129 199 200 204 227 230 231 298 303 328 330 332 333').split()))
print is_consistent(generate_linear_spectrum('VAQ'), map(int, str('0 71 99 101 103 128 129 199 200 204 227 230 231 298 303 328 330 332 333').split()))
print is_consistent(generate_linear_spectrum('CTV'), map(int, str('0 71 99 101 103 128 129 199 200 204 227 230 231 298 303 328 330 332 333').split()))
print is_consistent(generate_linear_spectrum('TVQ'), map(int, str('0 71 99 101 103 128 129 199 200 204 227 230 231 298 303 328 330 332 333').split()))
print is_consistent(generate_linear_spectrum('AQV'), map(int, str('0 71 99 101 103 128 129 199 200 204 227 230 231 298 303 328 330 332 333').split()))
print is_consistent(generate_linear_spectrum('TCE'), map(int, str('0 71 99 101 103 128 129 199 200 204 227 230 231 298 303 328 330 332 333').split()))

f.close()