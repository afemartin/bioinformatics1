# -*- coding: utf-8 -*-


import collections
import time


def calculate_mass(peptide):

    mass = 0

    if len(peptide) > 0:
        mass = sum([int(n) for n in peptide.split('-')])

    return mass


def parent_mass(spectrum):

    return max(spectrum)


def expand(peptides, alphabet):

    expanded = []

    for peptide in peptides:
        for aminoacid in alphabet:
            expanded.append(peptide + '-' + aminoacid)

    return expanded


def generate_linear_spectrum(peptide):

    # print peptide

    peptide = [int(n) for n in peptide.split('-')]

    peptide_length = len(peptide)

    i = 0

    masses = []

    while i <= peptide_length:
        if i == 0:
            masses.append(calculate_mass(''))
        elif i < peptide_length:
            for j in range(0, peptide_length - i + 1):
                subpeptide = '-' . join(str(n) for n in peptide[j:j+i])
                # print subpeptide
                masses.append(calculate_mass(subpeptide))
        else:
            subpeptide = '-' . join(str(n) for n in peptide)
            # print subpeptide
            masses.append(calculate_mass(subpeptide))
        i += 1

    return masses


def generate_cyclic_spectrum(peptide):

    # print peptide

    peptide = [int(n) for n in peptide.split('-')]

    peptide_length = len(peptide)

    peptide_sample = peptide + peptide

    i = 0

    masses = []

    while i <= peptide_length:
        if i == 0:
            masses.append(calculate_mass(''))
        elif i < peptide_length:
            for j in range(0, peptide_length):
                subpeptide = '-' . join(str(n) for n in peptide_sample[j:j+i])
                # print subpeptide
                masses.append(calculate_mass(subpeptide))
        else:
            subpeptide = '-' . join(str(n) for n in peptide)
            # print subpeptide
            masses.append(calculate_mass(subpeptide))
        i += 1

    return masses


def linear_score(peptide, spectrum):

    peptide_linear_spectrum = generate_linear_spectrum(peptide)

    # print peptide_linear_spectrum

    peptide_linear_spectrum_unique = list(set(peptide_linear_spectrum))

    # print peptide_linear_spectrum_unique

    c1 = collections.Counter(peptide_linear_spectrum)
    c2 = collections.Counter(spectrum)

    # print c1
    # print c2

    score = 0

    for aminoacid in peptide_linear_spectrum_unique:
        score += min(c1[aminoacid], c2[aminoacid])
        # print score

    return score


def cyclic_score(peptide, spectrum):

    peptide_cyclic_spectrum = generate_cyclic_spectrum(peptide)

    peptide_cyclic_spectrum_unique = list(set(peptide_cyclic_spectrum))

    c1 = collections.Counter(peptide_cyclic_spectrum)
    c2 = collections.Counter(spectrum)

    # print c1
    # print c2

    score = 0

    for aminoacid in peptide_cyclic_spectrum_unique:
        score += min(c1[aminoacid], c2[aminoacid])

    return score


def trim(leaderboard, spectrum, n):

    linear_scores = []
    trimmed_leaderboard = []

    leaderboard_length = len(leaderboard)

    leaderboard_scores = {}

    if n >= leaderboard_length:
        return leaderboard

    if leaderboard_length == 0 or n == 0:
        return trimmed_leaderboard

    for peptide in leaderboard:
        score = linear_score(peptide, spectrum)
        linear_scores.append(score)
        leaderboard_scores[peptide] = score

    linear_scores.sort(reverse=True)

    cut_value = linear_scores[n - 1]

    for peptide in leaderboard:
        if leaderboard_scores[peptide] >= cut_value:
            trimmed_leaderboard.append(peptide)

    return trimmed_leaderboard


def spectral_convolution(spectrum):

    spectrum_length = len(spectrum)

    frequent_aminoacids = []

    i = 0

    while i < spectrum_length:

        j = i + 1

        while j < spectrum_length:

            diff = spectrum[j] - spectrum[i]

            if diff > 0:
                frequent_aminoacids.append(spectrum[j] - spectrum[i])

            j += 1

        i += 1

    return frequent_aminoacids


def convolution_extended_alphabet(spectrum, m):

    alphabet = []

    convolution = spectral_convolution(spectrum)

    convolution = [mass for mass in convolution if 57 <= mass <= 200]

    c = collections.Counter(convolution)

    # for mass, repeats in c.most_common(m):
    #     alphabet.append(mass)

    cut_repeated_value = c.most_common()[m-1][1]

    for mass, repeats in c.most_common():
        if repeats >= cut_repeated_value:
            alphabet.append(mass)

    return alphabet


def convolution_cyclopeptide_sequence(spectrum, m, n):

    spectrum = sorted(map(int, spectrum.split()))

    spectrum_parent_mass = parent_mass(spectrum)

    alphabet = [str(mass) for mass in convolution_extended_alphabet(spectrum, m)]

    leaderboard = alphabet

    print leaderboard

    leader_peptide = ''
    leader_peptide_score = 0

    while len(leaderboard) > 0:

        leaderboard_iterable = list(leaderboard)

        for peptide in leaderboard_iterable:

            peptide_mass = calculate_mass(peptide)

            if peptide_mass == spectrum_parent_mass:
                peptide_score = cyclic_score(peptide, spectrum)
                if peptide_score > leader_peptide_score:
                    leader_peptide = peptide
                    leader_peptide_score = peptide_score
                    print peptide + ' (' + str(peptide_mass) + ')' + ' [' + str(peptide_score) + ']'
                leaderboard.remove(peptide)
            elif peptide_mass > spectrum_parent_mass:
                leaderboard.remove(peptide)

        leaderboard = trim(leaderboard, spectrum, n)

        leaderboard = expand(leaderboard, alphabet)

    return leader_peptide


f = open('dataset_104_7.txt', 'r')

example = f.read()

start = time.time()

# spectrum = '57 57 71 99 129 137 170 186 194 208 228 265 285 299 307 323 356 364 394 422 493'
# output = '99-71-137-57-72-57'
#
# print 'Leader peptide: ' + convolution_cyclopeptide_sequence(spectrum, 20, 60)
# print 'Expected output: ' + output + ' [' + str(cyclic_score(output, sorted(map(int, spectrum.split())))) + ']'


# spectrum = '853 113 585 796 924 502 423 1210 342 186 761 391 593 1412 1152 1396 260 129 1381 229 242 356 990 1047 57 748 1176 730 990 1038 1119 294 339 114 696 1251 1267 617 567 357 471 163 1266 1281 0 536 1395 454 1104 1362 1039 892 1509 1086 129 649 1095 713 258 777 1394 753 299 599 648 876 414 1249 813 242 859 1305 552 1284 861 650 1249 261 520 470 519 957 1233 405 260 861 762 810 1248 891 916 1346 390 981 147 1323 390 732 618 1380 1038 756 989 225 633 910 204 1452 243 1119 860 1395 129 57 503 1267 1153 276 462 228 1215 114 1170 357 973 388 519 699 131 128 1120 648 1452 1055 632 333 1380 528 747 389 656 97 1167 779 1380 1280 942 115 1121 1152 1007 990 1006 1118 519 877 1378 471'
# output = '113-115-114-128-97-163-131-129-129-147-57-57-129'
#
# print 'Leader peptide: ' + convolution_cyclopeptide_sequence(spectrum, 20, 373)
# print 'Expected output: ' + output + ' [' + str(cyclic_score(output, sorted(map(int, spectrum.split())))) + ']'


print 'Leader peptide: ' + convolution_cyclopeptide_sequence(example, 18, 328)

end = time.time()

print 'Execution time: ' + str(end - start) + ' segs'

f.close()
