# -*- coding: utf-8 -*-


def calculate_breakpoints(permutation):

    permutation = permutation[1:-1].split()

    length = len(permutation)

    permutation.insert(0, '+' + str(0))
    permutation.append('+' + str(length + 1))

    breakpoints = 0

    for k in range(0, length + 1):

        current_sign = permutation[k][0]
        current_value = int(permutation[k][1:])

        next_sign = permutation[k+1][0]
        next_value = int(permutation[k+1][1:])

        if current_sign != next_sign or abs(next_value - current_value) != 1:

            breakpoints += 1

        elif current_sign == '+' and current_value - next_value == 1:

            breakpoints += 1

        elif current_sign == '-' and current_value - next_value == -1:

            breakpoints += 1

    return breakpoints


f = open('dataset_287_4.txt', 'r')

example = f.read()

# print 'Breakpoints: ' + str(calculate_breakpoints('(+3 +4 +5 -12 -8 -7 -6 +1 +2 +10 +9 -11 +13 +14)')) + ' (expected output: 8)'

# print 'Breakpoints: ' + str(calculate_breakpoints(example)) + ' (expected output: 178)'

# print 'Breakpoints: ' + str(calculate_breakpoints(example))

# QUIZ 2 - QUESTION 3

print 'Breakpoints: ' + str(calculate_breakpoints('(-16 -20 +11 +12 -14 -13 -15 -6 -8 -19 -18 -17 -10 +4 -5 -2 +7 -3 +1 -9)'))

f.close()