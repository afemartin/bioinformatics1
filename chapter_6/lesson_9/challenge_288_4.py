# -*- coding: utf-8 -*-


def chromosome_to_cycle(chromosome):

    # ChromosomeToCycle(Chromosome)
    # for j ← 1 to |Chromosome|
    #      i ← Chromosomej
    #      if i > 0
    #           Node2j−1 ←2i−1
    #           Node2j ← 2i
    #      else
    #           Node2j−1 ← -2i
    #           Node2j ←-2i−1
    # return Nodes

    cycle = []

    for node in chromosome:

        node_sign = node[0]
        node_value = int(node[1:])

        if node_sign == '+':

            cycle.append(node_value * 2 - 1)
            cycle.append(node_value * 2)

        if node_sign == '-':

            cycle.append(node_value * 2)
            cycle.append(node_value * 2 - 1)

    return cycle


def colored_edges(genome):

    # ColoredEdges(P)
    # Edges ← an empty set
    # for each chromosome Chromosome in P
    #     Nodes ← ChromosomeToCycle(Chromosome)
    #     for j ← 1 to |Chromosome|
    #         add the edge (Nodes2j, Nodes2j +1) to Edges
    # return Edges

    edges = []

    for chromosome in genome:

        cycle = chromosome_to_cycle(chromosome)

        i = 1

        for node in chromosome:

            edges.append((cycle[(i*2-1) % len(cycle)], cycle[(i*2) % len(cycle)]))

            i += 1

    return edges


def two_break_distance(p, q):

    p_blocks = [block.split() for block in p[1:-1].split(')(')]
    q_blocks = [block.split() for block in q[1:-1].split(')(')]

    # print len(p_blocks)
    # print len(q_blocks)

    p_edges = colored_edges(p_blocks)
    q_edges = colored_edges(q_blocks)

    blocks = (len(p_edges) + len(q_edges)) / 2

    cycles = []

    current_cycle = []

    while len(p_edges) > 0 or len(q_edges) > 0:

        if len(current_cycle) == 0:

            current_genome = 'p'
            current_edge = p_edges.pop(0)
            current_node = current_edge[1]
            current_cycle.append(current_edge)

        # print current_genome
        # print current_edge
        # print current_node

        if current_genome == 'p':

            for edge in q_edges:

                if current_node in edge:

                    # save next current edge
                    current_edge = edge

                    # append current edge to current cycle
                    current_cycle.append(current_edge)

                    # remove edge for list of edges
                    q_edges.remove(edge)

                    # save next current node
                    current_node = edge[0] if edge[0] != current_node else edge[1]

                    # check if we rech the end of the cycle
                    if current_node == current_cycle[0][0]:

                        cycles.append(current_cycle)
                        current_cycle = []

                    break

        if current_genome == 'q':

            for edge in p_edges:

                if current_node in edge:

                    # save next current edge
                    current_edge = edge

                    # append current edge to current cycle
                    current_cycle.append(current_edge)

                    # remove edge for list of edges
                    p_edges.remove(edge)

                    # save next current node
                    current_node = edge[0] if edge[0] != current_node else edge[1]

                    # check if we rech the end of the cycle
                    if current_node == current_cycle[0][0]:

                        cycles.append(current_cycle)
                        current_cycle = []

                    break

        # print current_cycle

        current_genome = 'q' if current_genome == 'p' else 'p'

    # print cycles

    return blocks - len(cycles)


f = open('dataset_288_4.txt', 'r')

example = f.read().splitlines()

print 'Distance: ' + str(two_break_distance('(+1 +2 +3 +4 +5 +6)', '(+1 -3 -6 -5)(+2 -4)')) + ' (expected output: 3)'

# print 'Distance: ' + str(two_break_distance(example[0], example[1])) + ' (expected output: 9671)'

print 'Distance: ' + str(two_break_distance(example[0], example[1]))

f.close()