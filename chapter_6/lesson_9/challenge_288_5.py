# -*- coding: utf-8 -*-


import random


def get_black_edges(genome):

    # Sample Input:
    # (+1 -2 -3)(+4 +5 -6)
    # Sample Output:
    # (1, 2), (4, 3), (6, 5), (7, 8), (9, 10), (12, 11)

    # BlackEdges(P)
    # Edges ← an empty set
    # for each chromosome Chromosome in P
    #     Nodes ← ChromosomeToCycle(Chromosome)
    #     for j ← 1 to |Chromosome|
    #         add the edge (Nodes2j, Nodes2j +1) to Edges
    # return Edges

    edges = []

    for chromosome in genome:

        cycle = chromosome_to_cycle(chromosome)

        i = 0

        for node in chromosome:

            edges.append((cycle[(i*2) % len(cycle)], cycle[(i*2+1) % len(cycle)]))

            i += 1

    return edges


def get_colored_edges(genome):

    # Sample Input:
    # (+1 -2 -3)(+4 +5 -6)
    # Sample Output:
    # (2, 4), (3, 6), (5, 1), (8, 9), (10, 12), (11, 7)

    # ColoredEdges(P)
    # Edges ← an empty set
    # for each chromosome Chromosome in P
    #     Nodes ← ChromosomeToCycle(Chromosome)
    #     for j ← 1 to |Chromosome|
    #         add the edge (Nodes2j, Nodes2j +1) to Edges
    # return Edges

    edges = []

    for chromosome in genome:

        cycle = chromosome_to_cycle(chromosome)

        i = 1

        for node in chromosome:

            edges.append((cycle[(i*2-1) % len(cycle)], cycle[(i*2) % len(cycle)]))

            i += 1

    return edges


def chromosome_to_cycle(chromosome):

    # Sample Input:
    # (+1 -2 -3 +4)
    # Sample Output:
    # (1 2 4 3 6 5 7 8)

    # ChromosomeToCycle(Chromosome)
    # for j ← 1 to |Chromosome|
    #      i ← Chromosomej
    #      if i > 0
    #           Node2j−1 ←2i−1
    #           Node2j ← 2i
    #      else
    #           Node2j−1 ← -2i
    #           Node2j ←-2i−1
    # return Nodes

    cycle = []

    for node in chromosome:

        node_sign = node[0]
        node_value = int(node[1:])

        if node_sign == '+':

            cycle.append(node_value * 2 - 1)
            cycle.append(node_value * 2)

        if node_sign == '-':

            cycle.append(node_value * 2)
            cycle.append(node_value * 2 - 1)

    return cycle


def cycle_to_chromosome(cycle):

    # Sample Input:
    # (1 2 4 3 6 5 7 8)
    # Sample Output:
    # (+1 -2 -3 +4)

    # CycleToChromosome(Nodes)
    #  for j ← 1 to |Nodes|/2
    #       if Node2j−1 < Node2j
    #            Chromosomej ← Node2j /2
    #       else
    #            Chromosomej ← −Node2j−1/2
    #  return Chromosome

    chromosome = []

    for i in range(0, len(cycle)/2):

        if cycle[2*i] < cycle[2*i+1]:

            chromosome.append('+' + str(int(cycle[2*i+1])/2))

        else:

            chromosome.append('-' + str(int(cycle[2*i])/2))

    return chromosome


def graph_to_genome(graph):

    # Sample Input:
    # (2, 4), (3, 6), (5, 1), (7, 9), (10, 12), (11, 8)
    # Sample Output:
    # (+1 -2 -3)(-4 +5 -6)

    # GraphToGenome(GenomeGraph)
    #  P ← an empty set of chromosomes
    #  for each cycle Nodes in GenomeGraph
    #       Chromosome ← CycleToChromosome(Nodes)
    #       add Chromosome to P
    #  return P

    graph_iterable = list(graph)

    genome = []

    cycles = []

    current_cycle = []

    while len(graph_iterable) > 0:

        if len(current_cycle) == 0:

            graph_iterable.sort()

            current_edge = graph_iterable.pop(0)
            current_node = current_edge[1]
            current_cycle.append(current_edge[0])

        for edge in graph_iterable:

            if current_node in edge:

                # append current edge to current cycle
                current_cycle.append(current_node)

                # remove edge for list of edges
                graph_iterable.remove(edge)

                # save next current node
                current_node = edge[0] if edge[0] != current_node else edge[1]

                # check if we rech the end of the cycle
                if current_node == current_cycle[0]:

                    cycles.append(current_cycle)
                    current_cycle = []

                break

    for cycle in cycles:

        genome.append(cycle_to_chromosome(cycle))

    return genome


# def two_break_on_genome_graph(graph, i, ii, j, jj):
#
#     # Sample Input:
#     # (2, 4), (3, 8), (7, 5), (6, 1)
#     # 1, 6, 3, 8
#     # Sample Output:
#     # (2, 4), (3, 1), (7, 5), (6, 8)
#
#     # 2-BreakOnGenomeGraph(GenomeGraph, i, i′, j, j′)
#     #  remove colored edges (i, i') and (j, j′) from GenomeGraph
#     #  add colored edges (i, j) and (i′, j') to GenomeGraph
#     #  return GenomeGraph
#
#     # original = list(graph)
#
#     if (i, ii) in graph:
#         index = graph.index((i, ii))
#         graph[index] = (i, j)
#     else:
#         index = graph.index((ii, i))
#         graph[index] = (j, i)
#
#     if (j, jj) in graph:
#         index = graph.index((j, jj))
#         graph[index] = (ii, jj)
#     else:
#         index = graph.index((jj, j))
#         graph[index] = (jj, ii)
#
#     # if (i, ii) in graph:
#     #     index = graph.index((i, ii))
#     #     graph[index] = (jj, ii)
#     # else:
#     #     index = graph.index((ii, i))
#     #     graph[index] = (ii, jj)
#     #
#     # if (j, jj) in graph:
#     #     index = graph.index((j, jj))
#     #     graph[index] = (j, i)
#     # else:
#     #     index = graph.index((jj, j))
#     #     graph[index] = (i, j)
#
#     return graph


# def two_break_on_genome(genome, i, ii, j, jj):
#
#     # Sample Input:
#     # (+1 -2 -4 +3)
#     # 1, 6, 3, 8
#     # Sample Output:
#     # (+1 -2)(-3 +4)
#
#     # 2-BreakOnGenome(P, i, i′, j, j′)
#     #  GenomeGraph ← BlackEdges(P) and ColoredEdges(P)
#     #  GenomeGraph ← 2-BreakOnGenomeGraph(GenomeGraph, i, i′, j, j′)
#     #  P ← GraphToGenome(GenomeGraph)
#     #  return P
#
#     graph = get_black_edges(genome) + get_colored_edges(genome)
#
#     graph = two_break_on_genome_graph(graph, i, ii, j, jj)
#
#     genome = graph_to_genome(graph)
#
#     return genome


def get_breakpoint_graph(red_edges, blue_edges):

    graph_iterable = red_edges + blue_edges

    genome = []

    cycles = []

    current_cycle = []

    while len(graph_iterable) > 0:

        if len(current_cycle) == 0:

            current_edge = graph_iterable.pop(0)
            current_node = current_edge[1]
            current_cycle.append(current_edge[0])

        for edge in graph_iterable:

            if current_node in edge:

                # append current edge to current cycle
                current_cycle.append(current_node)

                # remove edge for list of edges
                graph_iterable.remove(edge)

                # save next current node
                current_node = edge[0] if edge[0] != current_node else edge[1]

                # check if we rech the end of the cycle
                if current_node == current_cycle[0]:

                    cycles.append(current_cycle)
                    current_cycle = []

                break

    for cycle in cycles:

        genome.append(cycle)

    return genome


def has_non_trivial_cycles(breakpoint_graph):

    for cycle in breakpoint_graph:

        if len(cycle) > 2:

            return True

    return False


def two_break_sorting(p, q):

    # Sample Input:
    # (+1 -2 -3 +4)
    # (+1 +2 -4 -3)
    # Sample Output:
    # (+1 -2 -3 +4)
    # (+1 -2 -3)(+4)
    # (+1 -2 -4 -3)
    # (-3 +1 +2 -4)

    # ShortestRearrangementScenario(P, Q)
    #  output P
    #  RedEdges ← ColoredEdges(P)
    #  BlueEdges ← ColoredEdges(Q)
    #  BreakpointGraph ← the graph formed by RedEdges and BlueEdges
    #  while BreakpointGraph has a non-trivial cycle Cycle
    #       (j, i′) ← an arbitrary edge from BlueEdges in a nontrivial red-blue cycle
    #       (i, j) ← an edge from RedEdges ending at node j
    #       (i′, j′) ← an edge from RedEdges ending at node i′
    #       RedEdges ← RedEdges with edges (i, j) and (i′, j′) removed
    #       RedEdges ← RedEdges with edges (j, i′) and (j′, i) added
    #       BreakpointGraph ← the graph formed by RedEdges and BlueEdges
    #       P ← 2-BreakOnGenome(P, i, i′, j, j′)
    #       output P

    p_blocks = [block.split() for block in p[1:-1].split(')(')]
    q_blocks = [block.split() for block in q[1:-1].split(')(')]

    # print 'p_blocks = ' + str(p_blocks)
    # print 'q_blocks = ' + str(q_blocks)

    black_edges = get_black_edges(p_blocks)

    red_edges = get_colored_edges(p_blocks)
    blue_edges = get_colored_edges(q_blocks)

    blue_edges_iterable = list(blue_edges)

    # print 'black_edges = ' + str(black_edges)
    # print 'red_edges = ' + str(red_edges)
    # print 'blue_edges = ' + str(blue_edges)

    genomes = [p_blocks]

    breakpoint_graph = get_breakpoint_graph(red_edges, blue_edges)
    # breakpoint_graph = get_breakpoint_graph(red_edges, black_edges)

    # print '(' + ')(' . join([' ' . join([str(i) for i in cycle]) for cycle in breakpoint_graph]) + ')'

    # print 'red_edges = ' + str(red_edges)
    # print 'blue_edges = ' + str(blue_edges)

    while has_non_trivial_cycles(breakpoint_graph) and len(blue_edges_iterable) > 0:

        found = 0

        while found != 2:

            found = 0

            if len(blue_edges_iterable) == 0:

                return ['ERROR !!!!!!!!!!!!!!!']

            arbitrary_blue_edge = random.choice(blue_edges_iterable)
            blue_edges_iterable.remove(arbitrary_blue_edge)

            # print '======================================='
            # print '(' + ')(' . join([' ' . join([str(i) for i in cycle]) for cycle in breakpoint_graph]) + ')'
            # print arbitrary_blue_edge
            # print '======================================='

            for red_edge in red_edges:

                if arbitrary_blue_edge[0] in red_edge:

                    red_edge_ending_j = red_edge

                    found += 1

                    break

            for red_edge in red_edges:

                if arbitrary_blue_edge[1] in red_edge and red_edge_ending_j != red_edge:

                    red_edge_ending_ii = red_edge

                    found += 1

                    break

        i = red_edge_ending_j[0]
        j = red_edge_ending_j[1]

        ii = red_edge_ending_ii[0]
        jj = red_edge_ending_ii[1]

        red_edges.remove(red_edge_ending_j)
        red_edges.remove(red_edge_ending_ii)

        red_edges.append((j, ii))
        red_edges.append((jj, i))

        genomes.append(graph_to_genome(red_edges + black_edges))

        breakpoint_graph = get_breakpoint_graph(red_edges, blue_edges)
        # breakpoint_graph = get_breakpoint_graph(red_edges, black_edges)

        # print '(' + ')(' . join([' ' . join([str(i) for i in cycle]) for cycle in breakpoint_graph]) + ')'

    # output formatting
    genomes = ['' . join(['(' + ' ' . join(block) + ')' for block in genome]) for genome in genomes]

    return genomes


f = open('dataset_288_5.txt', 'r')

example = f.read().splitlines()

# for i in range(0, 10):
#
#     print '2-Break Sorting:'
#     print '\n' . join(two_break_sorting('(+1 -2 -3 +4)', '(+1 +2 -4 -3)'))
#
# print '##############################'
# print '##############################'

# Output:
# (+1 -2 -3 +4)
# (+1 -2 -3)(+4)
# (+1 -2 -4 -3)
# (-3 +1 +2 -4)

# for i in range(0, 10):
#
#     print '2-Break Sorting:'
#     print '\n' . join(two_break_sorting('(-1 +4 -2 -3 +5)', '(+1 +2 +3 +4 +5)'))
#
# print '##############################'
# print '##############################'

# Output:
# (-1 +4 -2 -3 +5)
# (-1 +2 -4 -3 +5)
# (-1 +2 +3 +4 +5)
# (-1 -5 -4 -3 -2)

# for i in range(0, 10):
#
#     result = two_break_sorting('(+9 -8 +12 +7 +1 -14 +13 +3 -5 -11 +6 -2 +10 -4)', '(-11 +8 -10 -2 +3 +4 +13 +6 +12 +9 +5 +7 -14 -1)')
#
#     print '2-Break Sorting: length(' + str(len(result)) + ')'
#     print '\n' . join(result)
#
# print '##############################'
# print '##############################'

# Output
# (+9 -8 +12 +7 +1 -14 +13 +3 -5 -11 +6 -2 +10 -4)
# (+9 -8 +12 -3 -13 +14 -1 -7 -5 -11 +6 -2 +10 -4)
# (+9 -8 +12)(+7 +1 -14 +13 +3 +4 -10 +2 -6 +11 +5)
# (+9 -8 +12)(+1 -14 +13 +3 +4 -10 +2 -6 +11)(+5 +7)
# (+9 -8 +12)(+13 +3 +4 -10 +2 -6 +11 +1 +14)(+5 +7)
# (+9 -8 +12)(+13 +3 +4 -10 +2 -6 +11 +1 +14 -7 -5)
# (-8 +12 +9 +5 +7 -14 -1 -11 +6 -2 +10 -4 -3 -13)
# (-8 +12 +9 +5 +7 -14 -1 -11 +6 -2 +10)(+13 +3 +4)
# (+13 +3 +4)(-11 +6 +12 +9 +5 +7 -14 -1)(-2 +10 -8)
# (-2 +10 -8)(+5 +7 -14 -1 -11 +3 +4 +13 +6 +12 +9)
# (+5 +7 -14 -1 -11 +10 -8 -2 +3 +4 +13 +6 +12 +9)
# (+5 +7 -14 -1 -11 +8 -10 -2 +3 +4 +13 +6 +12 +9)

for i in range(0, 100):

    result = two_break_sorting(example[0], example[1])

    print '2-Break Sorting: length(' + str(len(result)) + ')'
    print '\n' . join(result)
    print '+++++++++++++++++++++++++++++++++++++++++++++++++'
    print example[1]

# Resultado correcto generado en una de las ejeciciones

# 2-Break Sorting: length(12)
# (+9 -6 +5 +7 -12 +10 +3 -4 -2 -11 +1 -8)
# (+1 -8 +9 -12 +10 +3 -4 -2 -11)(+5 +7 -6)
# (+1 -8 +9 -12 +10 +3 -11)(-2 -4)(+5 +7 -6)
# (+1 -8 +9 -5 +6 -7 -12 +10 +3 -11)(-2 -4)
# (+1 -8)(-2 -4)(+3 -11 +9 -5 +6 -7 -12 +10)
# (+1 +10 +3 -11 +9 -5 +6 -7 -12 -8)(-2 -4)
# (+1 +10 +11 -3 +9 -5 +6 -7 -12 -8)(-2 -4)
# (+1 +10 +11 -5 +6 -7 -12 -8)(-2 -4)(+3 -9)
# (+1 +3 -9 +10 +11 -5 +6 -7 -12 -8)(-2 -4)
# (+1 -9 +10 +11 -5 +6 -7 -12 -8)(-2 -4)(+3)
# (+1 -9 +10 +11 -5 +6 -7 -12 -8)(-2 -4 -3)
# (+1 -9 +10 +11 -5 -3 -2 -4 +6 -7 -12 -8)
# +++++++++++++++++++++++++++++++++++++++++++++++++
# (+2 +3 +5 -11 -10 +9 -1 +8 +12 +7 -6 +4)

f.close()