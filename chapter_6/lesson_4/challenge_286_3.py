# -*- coding: utf-8 -*-


def element_reversal(element):

    if element[0] == '+':

        return '-' + element[1:]

    else:

        return '+' + element[1:]


def k_sorting_reversal(permutation, k, length):

    element = str(k + 1)

    for l in range(k, length):

        if element == permutation[l][1:]:

            permutation_reversed = permutation[k:l+1]
            permutation_reversed.reverse()

            for i in range(0, len(permutation_reversed)):

                permutation[k+i] = element_reversal(permutation_reversed[i])

            break


def greedy_sorting(permutation):

    # GREEDYSORTING(P)
    #     approxReversalDistance ← 0
    #     for k = 1 to |P|
    #         if element k is not sorted
    #             apply the k-sorting reversal to P
    #             approxReversalDistance ← approxReversalDistance + 1
    #         if k-th element of P is −k
    #             apply the k-sorting reversal to P
    #             approxReversalDistance ← approxReversalDistance + 1
    #     return approxReversalDistance

    permutation = permutation[1:-1].split()

    sequence = []

    distance = 0

    length = len(permutation)

    for k in range(0, length):

        if str(k + 1) != permutation[k][1:]:

            k_sorting_reversal(permutation, k, length)

            distance += 1

            sequence.append('(' + ' ' . join(permutation) + ')')

        if ('-' + str(k + 1)) == permutation[k]:

            k_sorting_reversal(permutation, k, length)

            distance += 1

            sequence.append('(' + ' ' . join(permutation) + ')')

    return sequence


f = open('dataset_286_3.txt', 'r')

example = f.read()

# print 'Greedy sorting:'
# print "\n" . join(greedy_sorting('(-3 +4 +1 +5 -2)'))

# print 'Greedy sorting:'
# print "\n" . join(greedy_sorting(example))

# QUIZ 2 - QUESTION 2

print 'Greedy sorting:'
print "\n" . join(greedy_sorting('(+6 -12 -9 +17 +18 -4 +5 -3 +11 +19 +14 +10 +8 +15 -13 +20 +2 +7 -16 -1)'))

print 'Greedy sorting steps: ' + str(len(greedy_sorting('(+6 -12 -9 +17 +18 -4 +5 -3 +11 +19 +14 +10 +8 +15 -13 +20 +2 +7 -16 -1)')))

f.close()