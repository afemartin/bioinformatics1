# -*- coding: utf-8 -*-


def lcs_backtrack(u, v, w):

    l = len(u)
    m = len(v)
    n = len(w)

    matrix = [[[0 for row in range(0, n+1)] for col in range(0, m+1)] for deep in range(0, l+1)]

    backtrack = [[[' ' for row in range(0, n)] for col in range(0, m)] for deep in range(0, l)]

    for i in range(1, l+1):

        for j in range(1, m+1):

            for k in range(1, n+1):

                # print [u[i-1], v[j-1], w[k-1]]

                # b = back ; f = front

                ib_jf_kf = matrix[i-1][j][k]
                if_jb_kf = matrix[i][j-1][k]
                if_jf_kb = matrix[i][j][k-1]

                ib_jb_kf = matrix[i-1][j-1][k]
                ib_jf_kb = matrix[i-1][j][k-1]
                if_jb_kb = matrix[i][j-1][k-1]

                ib_jb_kb = matrix[i-1][j-1][k-1]

                if u[i-1] == v[j-1] == w[k-1]:

                    ib_jb_kb += 1

                    # print 'u=' + str(u) + ' ; v=' + str(v) + ' ; w=' + str(w)
                    # print 'i=' + str(i) + ' ; j=' + str(j) + ' ; k=' + str(k)

                    # print [ib_jf_kf, if_jb_kf, if_jf_kb, ib_jb_kf, ib_jf_kb, ij_jb_kb, ib_jb_kb]

                matrix[i][j][k] = max([ib_jf_kf, if_jb_kf, if_jf_kb, ib_jb_kf, ib_jf_kb, if_jb_kb, ib_jb_kb])

                if matrix[i][j][k] == ib_jb_kb:

                    backtrack[i-1][j-1][k-1] = '***'

                if matrix[i][j][k] == ib_jf_kf:

                    backtrack[i-1][j-1][k-1] = '*--'

                if matrix[i][j][k] == if_jb_kf:

                    backtrack[i-1][j-1][k-1] = '-*-'

                if matrix[i][j][k] == if_jf_kb:

                    backtrack[i-1][j-1][k-1] = '--*'

                if matrix[i][j][k] == ib_jb_kf:

                    backtrack[i-1][j-1][k-1] = '**-'

                if matrix[i][j][k] == ib_jf_kb:

                    backtrack[i-1][j-1][k-1] = '*-*'

                if matrix[i][j][k] == if_jb_kb:

                    backtrack[i-1][j-1][k-1] = '-**'

                if matrix[i][j][k] == ib_jb_kb and u[i-1] == v[j-1] == w[k-1]:

                    backtrack[i-1][j-1][k-1] = '***'

    alignment_score = matrix[i][j][k]

    # i = 0
    # for row in matrix:
    #     print 'i = ' + str(i) + '\n'
    #     for col in row:
    #         print ' ' . join(['{:3}'.format(item) for item in col])
    #     print '\n'
    #     i += 1

    # i = 0
    # for row in backtrack:
    #     print 'i = ' + str(i) + '\n'
    #     for col in row:
    #         print '     ' + ' ' . join(['{:5}'.format(item) for item in col])
    #     print '\n'
    #     i += 1

    return [alignment_score, backtrack]


def output_lcs(backtrack, u, v, w, i, j, k, u_done, v_done, w_done, output):

    print 'i=' + str(i) + ' ; j=' + str(j) + ' ; k=' + str(k) + ' ; token=' + str(backtrack[i][j][k]) + ' ; u=' + str(u[i]) + ' ; v=' + str(v[j]) + ' ; w=' + str(w[k])

    u_index = i
    v_index = j
    w_index = k

    last_iteration = i == 0 and j == 0 and k == 0

    ###################################

    if (backtrack[i][j][k][0] == '*' or last_iteration) and not u_done:

        output[0] = u[i] + output[0]
        u_index -= 1

    else:

        output[0] = '-' + output[0]

    ###################################

    if (backtrack[i][j][k][1] == '*' or last_iteration) and not v_done:

        output[1] = v[j] + output[1]
        v_index -= 1

    else:

        output[1] = '-' + output[1]

    ###################################

    if (backtrack[i][j][k][2] == '*' or last_iteration) and not w_done:

        output[2] = w[k] + output[2]
        w_index -= 1

    else:

        output[2] = '-' + output[2]

    ###################################

    u_done = u_done or (backtrack[i][j][k][0] == '*' and i == 0)
    v_done = v_done or (backtrack[i][j][k][1] == '*' and j == 0)
    w_done = w_done or (backtrack[i][j][k][2] == '*' and k == 0)

    if i == 0 and j == 0 and k == 0:

        return

    elif i == 0 and j == 0:

        output_lcs(backtrack, u, v, w, i, j, k - 1, u_done, v_done, w_done, output)

    elif i == 0 and k == 0:

        output_lcs(backtrack, u, v, w, i, j - 1, k, u_done, v_done, w_done, output)

    elif j == 0 and k == 0:

        output_lcs(backtrack, u, v, w, i - 1, j, k, u_done, v_done, w_done, output)

    elif i == 0 and backtrack[i][j][k][1] == '*' and backtrack[i][j][k][2] == '*':

        output_lcs(backtrack, u, v, w, i, j - 1, k - 1, u_done, v_done, w_done, output)

    elif j == 0 and backtrack[i][j][k][0] == '*' and backtrack[i][j][k][2] == '*':

        output_lcs(backtrack, u, v, w, i - 1, j, k - 1, u_done, v_done, w_done, output)

    elif k == 0 and backtrack[i][j][k][0] == '*' and backtrack[i][j][k][1] == '*':

        output_lcs(backtrack, u, v, w, i - 1, j - 1, k, u_done, v_done, w_done, output)

    else:

        output_lcs(backtrack, u, v, w, u_index, v_index, w_index, u_done, v_done, w_done, output)

    return


def multiple_alignment(string1, string2, string3):

    lcs = lcs_backtrack(string1, string2, string3)

    aligment_score = str(lcs[0])
    backtrack = lcs[1]

    output = ['', '', '']

    output_lcs(backtrack, string1, string2, string3, len(string1) - 1, len(string2) - 1, len(string3) - 1, False, False, False, output)

    return [aligment_score, output[0], output[1], output[2]]


f = open('dataset_251_5.txt', 'r')

lines = f.read().splitlines()

print 'Multiple Longest Common Subsequence:'
print "\n" . join(multiple_alignment('ATATCCG', 'TCCGA', 'ATGTACTG'))

# Output:
# 3
# ATATCC-G-
# ---TCC-GA
# ATGTACTG-

print 'Multiple Longest Common Subsequence:'
print "\n" . join(multiple_alignment(lines[0], lines[1], lines[2]))

f.close()
