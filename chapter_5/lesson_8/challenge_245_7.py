# -*- coding: utf-8 -*-


import operator


def max_nodes(a, b, predecesors, path, iteration):

    iteration += 1

    weight_dict = {}

    if a == b:

        return 0

    if str(b) not in predecesors:

        return -999999

    print str(b) + ' -> ' + str(predecesors[str(b)])

    for node_prev, weight in predecesors[str(b)].iteritems():

        print weight + ' + max_nodes(' + str(a) + ', ' + node_prev + ')'

        weight_dict[node_prev] = int(weight) + max_nodes(a, int(node_prev), predecesors, path, iteration)

    max_weight = max(weight_dict.values())
    max_node = max(weight_dict.iteritems(), key=operator.itemgetter(1))[0]

    if max_weight > 0 and int(max_node) != a:

        path.append(int(max_node))

    return max_weight


def longest_path_dga(a, b, nodes):

    path = []

    predecesors = {}

    for node in nodes:

        data = node.split(':')

        node_prev = data[0].split('->')[0]
        node_next = data[0].split('->')[1]

        weight = data[1]

        if str(node_next) in predecesors:

            predecesors[str(node_next)][str(node_prev)] = weight

        else:

            predecesors[str(node_next)] = {str(node_prev): weight}

    for key, value in predecesors.iteritems():
        print key + ' -> ' + ', ' .join(value)

    weight = max_nodes(a, b, predecesors, path, 0)

    path.insert(0, a)

    path.append(b)

    return [str(weight), ' ' . join([str(i) for i in path])]


f = open('dataset_245_7_quiz.txt', 'r')

example = f.read().splitlines()

# print 'Longest Path DGA:'
# print "\n" . join(longest_path_dga(0, 4, example)) + ' (expected output: (9) 0->2->3->4)'

print 'Longest Path DGA: (quiz 5 - question 5)'
print "\n" . join(longest_path_dga(1, 7, example))
print 'Recuerda convertir los numeros a letras 1->a, 2->b, etc.'

# print 'Longest Path DGA:'
# print "\n" . join(longest_path_dga(0, 44, example)) + ' (expected output: (62) 0->14->29->44)'

# print 'Longest Path DGA:'
# print "\n" . join(longest_path_dga(1, 27, example))

f.close()