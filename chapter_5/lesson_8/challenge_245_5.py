# -*- coding: utf-8 -*-


import sys


sys.setrecursionlimit(5000)


def lcs_backtrack(v, w):

    # LCSBACKTRACK(v, w)
    #     for i ← 0 to |v|
    #         si, 0 ← 0
    #     for j ← 0 to |w|
    #         s0, j ← 0
    #     for i ← 1 to |v|
    #         for j ← 1 to |w|
    #             si, j ← max{si-1, j, si,j-1, si-1, j-1 + 1 (if vi = wj)}
    #             if si,j = si-1,j
    #                 Backtracki, j ← "↓"
    #             if si, j = si, j-1
    #                 Backtracki, j ← "→"
    #             if si, j = si-1, j-1 + 1
    #                 Backtracki, j ← "↘"
    #     return Backtrack

    m = len(v)
    n = len(w)

    matrix = [[0 for row in range(0, n+1)] for col in range(0, m+1)]

    backtrack = [[' ' for row in range(0, n)] for col in range(0, m)]

    for i in range(1, m+1):

        for j in range(1, n+1):

            diagonal = matrix[i-1][j-1]

            # print str(len(v)) + '>' + str(i) + ' V'
            # print str(len(w)) + '>' + str(j) + ' W'

            if v[i-1] == w[j-1]:

                diagonal += 1

            matrix[i][j] = max([matrix[i-1][j], matrix[i][j-1], diagonal])

            # print str(matrix[i][j]) + ' = max(' + str(matrix[i-1][j]) + ', ' + str(matrix[i][j-1]) + ', ' + str(diagonal) + ')'

            if matrix[i][j] == matrix[i][j-1]:

                backtrack[i-1][j-1] = '>'

            if matrix[i][j] == matrix[i-1][j]:

                backtrack[i-1][j-1] = 'V'

            if v[i-1] == w[j-1]:

                backtrack[i-1][j-1] = '*'

    for row in matrix:
        print row
    print ''

    for row in backtrack:
        print row
    print ''

    return backtrack


def output_lcs(backtrack, v, w, i, j):

    # OUTPUTLCS(backtrack, v, i, j)
    #     if i = 0 or j = 0
    #         return
    #     if backtracki, j = "↓"
    #         OUTPUTLCS(backtrack, v, i - 1, j)
    #     else if backtracki, j = "→"
    #         OUTPUTLCS(backtrack, v, i, j - 1)
    #     else
    #         OUTPUTLCS(backtrack, v, i - 1, j - 1)
    #         output vi

    output = ''

    if i == 0 and j == 0:

        if backtrack[i][j] == '*':

            return v[i]

        else:

            return ''

    print 'i=' + str(i) + ' ; j=' + str(j) + ' ; v=' + str(v[i]) + ' ; w=' + str(w[j])

    if i == 0:

        output = output_lcs(backtrack, v, w, i, j - 1)

    elif j == 0:

        output = output_lcs(backtrack, v, w, i - 1, j)

    elif backtrack[i][j] == 'V':

        output = output_lcs(backtrack, v, w, i - 1, j)

    elif backtrack[i][j] == '>':

        output = output_lcs(backtrack, v, w, i, j - 1)

    else:

        output = output_lcs(backtrack, v, w, i - 1, j - 1)

        output += v[i]

    return output


def longest_common_subsequence(string1, string2):

    i = len(string1) - 1
    j = len(string2) - 1

    backtrack = lcs_backtrack(string1, string2)

    return output_lcs(backtrack, string1, string2, i, j)


f = open('dataset_245_5.txt', 'r')

lines = f.read().splitlines()

print 'Longest Common Subsequence:'
print longest_common_subsequence('AACCTTGG', 'ACACTGTGA') + ' (expected output: AACTGG)'
print longest_common_subsequence('ACACTGTGA', 'AACCTTGG') + ' (expected output: AACTGG)'

print 'Longest Common Subsequence: (quiz 5 - question 1)'
print longest_common_subsequence('TGTACG', 'GCTAGT')
print longest_common_subsequence('GCTAGT', 'TGTACG')

# print 'Longest Common Subsequence:'
# print longest_common_subsequence(lines[1], lines[0])

f.close()