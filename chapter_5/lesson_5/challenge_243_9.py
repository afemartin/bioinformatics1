# -*- coding: utf-8 -*-


def dinamic_programming_change(money, coins):

    min_num_coins = {'0': 0}

    for m in range(1, money + 1):

        min_num_coins[str(m)] = 9876543210

        for coin in coins:

            if m >= coin and min_num_coins[str(m - coin)] + 1 < min_num_coins[str(m)]:

                min_num_coins[str(m)] = min_num_coins[str(m - coin)] + 1

    return min_num_coins[str(money)]


# f = open('dataset_243_9.txt', 'r')

# example = f.read()

print 'DP Minimum number of coins: ' + str(dinamic_programming_change(40, [50, 25, 20, 10, 5, 1]))

print 'DP Minimum number of coins: ' + str(dinamic_programming_change(8074, [24, 13, 12, 7, 5, 3, 1]))

print 'DP Minimum number of coins: ' + str(dinamic_programming_change(17582, [16, 14, 11, 5, 3, 1]))


# f.close()