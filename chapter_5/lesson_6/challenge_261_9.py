# -*- coding: utf-8 -*-


def manhattan_tourist(n, m, down, right):

#     MANHATTANTOURIST(n, m, Down, Right)
#         s0, 0 ← 0
#         for i ← 1 to n
#             si, 0 ← si-1, 0 + downi, 0
#         for j ← 1 to m
#             s0, j ← s0, j−1 + right0, j
#         for i ← 1 to n
#             for j ← 1 to m
#                 si, j ← max{si - 1, j + downi, j, si, j - 1 + righti, j}
#         return sn, m

    matrix = [[0 for row in range(0, m+1)] for col in range(0, n+1)]

    # for row in matrix:
    #     print row
    # print ''

    for i in range(1, n+1):

        matrix[i][0] = matrix[i-1][0] + int(down[i-1][0])

    # for row in matrix:
    #     print row
    # print ''

    for j in range(1, m+1):

        matrix[0][j] = matrix[0][j-1] + int(right[0][j-1])

    # for row in matrix:
    #     print row
    # print ''

    for i in range(1, n+1):

        for j in range(1, m+1):

            matrix[i][j] = max(matrix[i-1][j] + int(down[i-1][j]), matrix[i][j-1] + int(right[i][j-1]))

    # for row in matrix:
    #     print row
    # print ''

    return matrix[n][m]


f = open('dataset_261_9_sample.txt', 'r')

lines = f.read().splitlines()

down = []
right = []

matrix = down

for line in lines:

    if line == '-':

        matrix = right
        continue

    matrix.append(line.split())

print 'Length of the longest path: ' + str(manhattan_tourist(4, 4, down, right)) + ' (expected output: 34)'

f.close()


f = open('dataset_261_9_extra.txt', 'r')

lines = f.read().splitlines()

down = []
right = []

matrix = down

for line in lines:

    if line == '-':

        matrix = right
        continue

    matrix.append(line.split())

print 'Length of the longest path: ' + str(manhattan_tourist(17, 9, down, right)) + ' (expected output: 84)'

f.close()


f = open('dataset_261_9.txt', 'r')

lines = f.read().splitlines()

down = []
right = []

matrix = down

for line in lines:

    if line == '-':

        matrix = right
        continue

    matrix.append(line.split())

print 'Length of the longest path: ' + str(manhattan_tourist(17, 19, down, right))

f.close()