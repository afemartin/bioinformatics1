# -*- coding: utf-8 -*-


import sys


sys.setrecursionlimit(5000)


f = open('PAM250.txt', 'r')

lines = f.read().splitlines()

indel_penalty = 5

pam_250 = {}

indexes = lines[0].split()

for x in indexes:

    pam_250[x] = {}

    for y in indexes:

        pam_250[x][y] = int(lines[indexes.index(x)+1][2:].split()[indexes.index(y)])

# for row in pam_250.iteritems():
#     print row

f.close()


def lcs_backtrack(v, w):

    m = len(v)
    n = len(w)

    matrix = [[0 for row in range(0, m+1)] for col in range(0, n+1)]

    backtrack = [[' ' for row in range(0, m)] for col in range(0, n)]

    alignment_score = 0

    for i in range(1, n+1):

        for j in range(1, m+1):

            free_ride_from_source = 0

            down = matrix[i-1][j] - indel_penalty

            right = matrix[i][j-1] - indel_penalty

            diagonal = matrix[i-1][j-1]

            # print str(len(v)) + '>' + str(i) + ' V'
            # print str(len(w)) + '>' + str(j) + ' W'

            if v[j-1] == w[i-1]:

                # diagonal += 1
                diagonal += pam_250[w[i-1]][v[j-1]]

            else:

                # wtf!? pensaba que habia que restar el valor de la matriz, pero hay que sumarlo
                # independientemente de si coiciden o no los valores en la digonal
                diagonal += pam_250[w[i-1]][v[j-1]]

            matrix[i][j] = max([free_ride_from_source, down, right, diagonal])

            # print str(matrix[i][j]) + ' = max(' + str(down) + ', ' + str(right) + ', ' + str(diagonal) + ')'

            # print str(i) + ' - ' + str(n) + ' - ' + str(w)
            # print str(j) + ' - ' + str(m) + ' - ' + str(v)

            if matrix[i][j] == free_ride_from_source:

                backtrack[i-1][j-1] = '-'
                # print '(' + str(i) + ', ' + str(j) + '), Comparing ' + w[i-1] + ' ' + v[j-1] + ', down = ' + str(matrix[i-1][j]) + ' - 5 = ' + str(down) + ', right = ' + str(matrix[i][j-1]) + ' - 5 = ' + str(right) + ', diag. = ' + str(matrix[i-1][j-1]) + ' + ' + str(pam_250[w[i-1]][v[j-1]]) + ' = ' + str(diagonal) + ', choose: free taxi ride from bottom'

            if matrix[i][j] == diagonal:

                backtrack[i-1][j-1] = '*'
                # print '(' + str(i) + ', ' + str(j) + '), Comparing ' + w[i-1] + ' ' + v[j-1] + ', down = ' + str(matrix[i-1][j]) + ' - 5 = ' + str(down) + ', right = ' + str(matrix[i][j-1]) + ' - 5 = ' + str(right) + ', diag. = ' + str(matrix[i-1][j-1]) + ' + ' + str(pam_250[w[i-1]][v[j-1]]) + ' = ' + str(diagonal) + ', choose: diag.'

            if matrix[i][j] == right:

                backtrack[i-1][j-1] = '>'
                # print '(' + str(i) + ', ' + str(j) + '), Comparing ' + w[i-1] + ' ' + v[j-1] + ', down = ' + str(matrix[i-1][j]) + ' - 5 = ' + str(down) + ', right = ' + str(matrix[i][j-1]) + ' - 5 = ' + str(right) + ', diag. = ' + str(matrix[i-1][j-1]) + ' + ' + str(pam_250[w[i-1]][v[j-1]]) + ' = ' + str(diagonal) + ', choose: right'

            if matrix[i][j] == down:

                backtrack[i-1][j-1] = 'V'
                # print '(' + str(i) + ', ' + str(j) + '), Comparing ' + w[i-1] + ' ' + v[j-1] + ', down = ' + str(matrix[i-1][j]) + ' - 5 = ' + str(down) + ', right = ' + str(matrix[i][j-1]) + ' - 5 = ' + str(right) + ', diag. = ' + str(matrix[i-1][j-1]) + ' + ' + str(pam_250[w[i-1]][v[j-1]]) + ' = ' + str(diagonal) + ', choose: down'

            if matrix[i][j] >= alignment_score:

                alignment_score = matrix[i][j]

                free_ride_to_sink_i = i
                free_ride_to_sink_j = j

    # for i in range(1, n+1):
    #
    #     for j in range(1, m+1):
    #
    #         if i > free_ride_to_sink_i and j > free_ride_to_sink_j:
    #
    #             backtrack[i-1][j-1] = '-'

    # for row in matrix:
    #     print row
    # print ''

    # for row in backtrack:
    #     print row
    # print ''

    return [alignment_score, backtrack, free_ride_to_sink_i, free_ride_to_sink_j]


def output_lcs(backtrack, v, w, i, j, output):

    if i == 0 and j == 0:

        if backtrack[i][j] == '*':

            output[0] = v[i] + output[0]
            output[1] = w[j] + output[1]

            return

        else:

            return

    if backtrack[i][j] == '-':

        return

    elif i == 0:

        output[0] = '-' + output[0]
        output[1] = w[j] + output[1]

        output_lcs(backtrack, v, w, i, j - 1, output)

    elif j == 0:

        output[0] = v[i] + output[0]
        output[1] = '-' + output[1]

        output_lcs(backtrack, v, w, i - 1, j, output)

    elif backtrack[i][j] == 'V':

        output[0] = v[i] + output[0]
        output[1] = '-' + output[1]

        output_lcs(backtrack, v, w, i - 1, j, output)

    elif backtrack[i][j] == '>':

        output[0] = '-' + output[0]
        output[1] = w[j] + output[1]

        output_lcs(backtrack, v, w, i, j - 1, output)

    else:

        output[0] = v[i] + output[0]
        output[1] = w[j] + output[1]

        output_lcs(backtrack, v, w, i - 1, j - 1, output)

    return


def global_alignment(string1, string2):

    lcs = lcs_backtrack(string2, string1)

    alignment_score = str(lcs[0])
    backtrack = lcs[1]

    output = ['', '']

    output_lcs(backtrack, string1, string2, lcs[2] - 1, lcs[3] - 1, output)

    return [alignment_score, output[0], output[1]]


f = open('dataset_247_9.txt', 'r')

lines = f.read().splitlines()

# print 'Global Alignment Problem: (expected output: 15 / EANL-Y / ENALTY)'
# print "\n" . join(global_alignment('MEANLY', 'PENALTY'))

# print 'Global Alignment Problem: (expected output: 1062 / ... / ...)'
# print "\n" . join(global_alignment(lines[0], lines[1]))

# Output:
# 1062
# YQAGIIRQPPRGD-RGVSDRNYSQCGKQ-NQ-AQLDNNPTWTKYEIEWRVQI-LPPGAGVFEGDNGQNQCLCPNW--A-W-EQPCQW----GALHS-NEQYPNRIHLWAPMSKLHIKIEKSSYN-RNAQ-FPNRCMYECE-FPSY-REQVDSCHYENVQIAF-TIFSGAEQKRKFCSCHFWSNFIDQAVFSTGLI-PWCYRRDDHSAFFMPNWNKQ--YKHPQLQFRVAGEGTQCRPFYTREMFTKVSAWRIAGRFAGPYERHHDAHLELWY-QHHKVRT-GQQLGIIWNNRDKTRNPCPFSAY-Y-NK--LP-WWK-I-NQ-N-AFYNCLQNIAHSTHDETHEFNPVKCIDWLQGTMV-P------TECKKGFVHEKCECYRNPGPPLHDMYHQMEDIFGVRFDCLTGWKHLS------D---YNPC-QERRNINDFYIFAYEIAPAVKNLVLSPQPLADATKKCAFNYTPLDQSPVVIACK---WYIHQPI-CMLL----IVLIC-AMDKYNAHMIVIRTTEGQQPMHACRMTEGPGMCMKEPLVTFTLPAQWQWPNHEFKYVYMYVLNYHLSQYTYTDEGHAGGQHYSFNVAVDVGMAWGHNRCYCQPACYSQQETQTRTIDYEKWQYMKHQAFKWGLWFCEQER-HA--WFKGQNRCEMFTAKMTRMGADSNLDQYKLMLAQNYEEQWEQPIMECGMSEIIEIDPPYRSELIFTFWPFCTYSPWQNLIKCRCNNVIEEMDQCVP-LTF-IGFGVKQAGGIQA-WAFYKE--EWTSTYYLMCQCMKSDKAQYPYEIILFWMQ--P-MDTGE--QEPPQQNMWIFLPHSWFFDWCCNAPWSEICSSRHD--H---GQ-CQDAFYPCELFTVF
# Y-P-MSRKTAKSQFIEWCDW-F--CFNHWTNWAPLSIVRTSVAFAV-W-GHCWYPCG-GVCKTNRCKDD-FCGRWRKALFAEGPRDWKCCKNDLQNWNPQYSQGTR--NTK-RMVATTNQTMIEWKQSHIFETW-LF-CHVIIEYNWSAF-W-MWMNRNEAFNSIIKSGYPKLLL-T-QY-P-L-SQG--STPIVKPL-IRRD-QGKFW-A-WAQMWWFREPT-NIPTA-D-Y-CHSW--WQ--SR-ADLQ-NDRDMGP-EADASFYVEFWYWVRCAARTYGQQLGIIWNNRLKTRNPCPYSADGIQNKENYVFWWKNMCTKSHIAFYYCLQNVAHYTHDVTAEFNPVKCIDWLQGHMVLSSWFKYNTECKKLFVHEKCECYRM----FCGV---VEDIFGVRFH--TGWKHLSTAKPVPHVCVYNPSVQERRNINDFYIF-YEIAPAVKNLVLSAQPLHDYTKKCAFNYTPITITRIISTRNQIIW-AHVVIACQFYSPHQMLLIELAMDKYCADMNVRRSTEGHQPMHACRSTFGPGMAAKEPLVTFTLVAFWQWPNHEFQYVYMYTED-KIIQIG-PHLSN-GCEMVEYCVDC-YAK-RPCYRAYSAEAQYWRMITEAEDYSYKTRNAIAATATVRGQ-YCHPFRWLGIVWM-AHHDC-FFANECGTICI-PQMAEMRPPETTPYEI--DIIFMMF-WKE--HMSTTIL-DVVGMYRP-ATFSHWHDAHH-QCEPYLTPL-MCQSKLVFDAAFT--QVG-VKGVW-YHTEKLELMAGFNHM-K-FKKEEAQ---QSCFYWFQDCPDYDPPDAVRKTDEKHIRAHGEIWWLMRYYCMYHILHI-ASRHEWMHLRWDQACTNPGY--ELFE-F

print 'Global Alignment Problem:'
print "\n" . join(global_alignment(lines[0], lines[1]))

f.close()
