# -*- coding: utf-8 -*-


import sys


sys.setrecursionlimit(5000)


def lcs_backtrack(v, w):

    m = len(v)
    n = len(w)

    matrix = [[0 for row in range(0, m+1)] for col in range(0, n+1)]

    backtrack = [[' ' for row in range(0, m)] for col in range(0, n)]

    for i in range(1, n+1):

        for j in range(1, m+1):

            down = matrix[i-1][j]

            right = matrix[i][j-1]

            diagonal = matrix[i-1][j-1] + 1

            # print str(len(v)) + '>' + str(i) + ' V'
            # print str(len(w)) + '>' + str(j) + ' W'

            if v[j-1] == w[i-1]:

                diagonal += 1

            matrix[i][j] = max([down, right, diagonal])

            # print str(matrix[i][j]) + ' = max(' + str(down) + ', ' + str(right) + ', ' + str(diagonal) + ')'

            # print str(i) + ' - ' + str(n) + ' - ' + str(w)
            # print str(j) + ' - ' + str(m) + ' - ' + str(v)

            if matrix[i][j] == down:

                backtrack[i-1][j-1] = 'V'
                # print '(' + str(i) + ', ' + str(j) + '), Comparing ' + w[i-1] + ' ' + v[j-1] + ', down = ' + str(matrix[i-1][j]) + ' - 5 = ' + str(down) + ', right = ' + str(matrix[i][j-1]) + ' - 5 = ' + str(right) + ', diag. = ' + str(matrix[i-1][j-1]) + ' + ' + str(blosum_62[w[i-1]][v[j-1]]) + ' = ' + str(diagonal) + ', choose: down'

            if matrix[i][j] == right:

                backtrack[i-1][j-1] = '>'
                # print '(' + str(i) + ', ' + str(j) + '), Comparing ' + w[i-1] + ' ' + v[j-1] + ', down = ' + str(matrix[i-1][j]) + ' - 5 = ' + str(down) + ', right = ' + str(matrix[i][j-1]) + ' - 5 = ' + str(right) + ', diag. = ' + str(matrix[i-1][j-1]) + ' + ' + str(blosum_62[w[i-1]][v[j-1]]) + ' = ' + str(diagonal) + ', choose: right'

            if matrix[i][j] == diagonal:

                backtrack[i-1][j-1] = '*'
                # print '(' + str(i) + ', ' + str(j) + '), Comparing ' + w[i-1] + ' ' + v[j-1] + ', down = ' + str(matrix[i-1][j]) + ' - 5 = ' + str(down) + ', right = ' + str(matrix[i][j-1]) + ' - 5 = ' + str(right) + ', diag. = ' + str(matrix[i-1][j-1]) + ' + ' + str(blosum_62[w[i-1]][v[j-1]]) + ' = ' + str(diagonal) + ', choose: diag.'

    # for row in matrix:
    #     print row
    # print ''

    # for row in backtrack:
    #     print row
    # print ''

    return backtrack


def output_lcs(backtrack, v, w, i, j, output):

    if i == 0 and j == 0:

        if backtrack[i][j] == '*':

            output[0] = v[i] + output[0]
            output[1] = w[j] + output[1]

            return

        else:

            return

    if i == 0:

        output[0] = '-' + output[0]
        output[1] = w[j] + output[1]

        output_lcs(backtrack, v, w, i, j - 1, output)

    elif j == 0:

        output[0] = v[i] + output[0]
        output[1] = '-' + output[1]

        output_lcs(backtrack, v, w, i - 1, j, output)

    elif backtrack[i][j] == 'V':

        output[0] = v[i] + output[0]
        output[1] = '-' + output[1]

        output_lcs(backtrack, v, w, i - 1, j, output)

    elif backtrack[i][j] == '>':

        output[0] = '-' + output[0]
        output[1] = w[j] + output[1]

        output_lcs(backtrack, v, w, i, j - 1, output)

    else:

        output[0] = v[i] + output[0]
        output[1] = w[j] + output[1]

        output_lcs(backtrack, v, w, i - 1, j - 1, output)

    return


def edit_distance(string1, string2):

    backtrack = lcs_backtrack(string2, string1)

    distance = 0

    output = ['', '']

    output_lcs(backtrack, string1, string2, len(string1) - 1, len(string2) - 1, output)

    for i in range(0, len(output[0])):

        if output[0][i] != output[1][i]:

            distance += 1

    return distance


f = open('dataset_248_3.txt', 'r')

lines = f.read().splitlines()

# print 'Edit Distance Problem: (expected output: 5)'
# print edit_distance('PLEASANTLY', 'MEANLY')

# print 'Edit Distance Problem: (expected output: 400)'
# print edit_distance(lines[0], lines[1])

print 'Edit Distance Problem:'
print edit_distance(lines[0], lines[1])

f.close()
