# -*- coding: utf-8 -*-


import sys


sys.setrecursionlimit(5000)


def lcs_backtrack(v, w):

    m = len(v)
    n = len(w)

    matrix = [[0 for row in range(0, m+1)] for col in range(0, n+1)]

    backtrack = [[' ' for row in range(0, m)] for col in range(0, n)]

    alignment_score = 0

    for j in range(1, m+1):

        matrix[0][j] = matrix[0][j-1] - 1

    for i in range(1, n+1):

        for j in range(1, m+1):

            free_ride_from_source = 0

            down = matrix[i-1][j] - 2

            right = matrix[i][j-1] - 2

            if v[j-1] == w[i-1]:

                diagonal = matrix[i-1][j-1] + 1

            else:

                diagonal = matrix[i-1][j-1] - 2

            if j == 1:

                matrix[i][j] = max([free_ride_from_source, down, right, diagonal])

            else:

                matrix[i][j] = max([down, right, diagonal])

            # print str(matrix[i][j]) + ' = max(' + str(down) + ', ' + str(right) + ', ' + str(diagonal) + ')'

            # print str(i) + ' - ' + str(n) + ' - ' + str(w)
            # print str(j) + ' - ' + str(m) + ' - ' + str(v)

            if matrix[i][j] == free_ride_from_source:

                backtrack[i-1][j-1] = '-'
                # print '(' + str(i) + ', ' + str(j) + '), Comparing ' + w[i-1] + ' ' + v[j-1] + ', down = ' + str(matrix[i-1][j]) + ' - 5 = ' + str(down) + ', right = ' + str(matrix[i][j-1]) + ' - 5 = ' + str(right) + ', diag. = ' + str(matrix[i-1][j-1]) + ' + ' + str(pam_250[w[i-1]][v[j-1]]) + ' = ' + str(diagonal) + ', choose: free taxi ride from bottom'

            if matrix[i][j] == right:

                backtrack[i-1][j-1] = '>'
                # print '(' + str(i) + ', ' + str(j) + '), Comparing ' + w[i-1] + ' ' + v[j-1] + ', down = ' + str(matrix[i-1][j]) + ' - 5 = ' + str(down) + ', right = ' + str(matrix[i][j-1]) + ' - 5 = ' + str(right) + ', diag. = ' + str(matrix[i-1][j-1]) + ' + ' + str(pam_250[w[i-1]][v[j-1]]) + ' = ' + str(diagonal) + ', choose: right'

            if matrix[i][j] == down:

                backtrack[i-1][j-1] = 'V'
                # print '(' + str(i) + ', ' + str(j) + '), Comparing ' + w[i-1] + ' ' + v[j-1] + ', down = ' + str(matrix[i-1][j]) + ' - 5 = ' + str(down) + ', right = ' + str(matrix[i][j-1]) + ' - 5 = ' + str(right) + ', diag. = ' + str(matrix[i-1][j-1]) + ' + ' + str(pam_250[w[i-1]][v[j-1]]) + ' = ' + str(diagonal) + ', choose: down'

            if matrix[i][j] == diagonal:

                backtrack[i-1][j-1] = '*'
                # print '(' + str(i) + ', ' + str(j) + '), Comparing ' + w[i-1] + ' ' + v[j-1] + ', down = ' + str(matrix[i-1][j]) + ' - 5 = ' + str(down) + ', right = ' + str(matrix[i][j-1]) + ' - 5 = ' + str(right) + ', diag. = ' + str(matrix[i-1][j-1]) + ' + ' + str(pam_250[w[i-1]][v[j-1]]) + ' = ' + str(diagonal) + ', choose: diag.'

            if matrix[i][j] >= alignment_score and i == n:

                alignment_score = matrix[i][j]

                free_ride_to_sink_i = i
                free_ride_to_sink_j = j

    # for row in matrix:
    #     print ' ' . join(['{:3}'.format(item) for item in row])
    # print ''

    # for row in backtrack:
    #     print '      ' + ' ' . join(['{:3}'.format(item) for item in row])
    # print ''

    return [alignment_score, backtrack, free_ride_to_sink_i, free_ride_to_sink_j]


def output_lcs(backtrack, v, w, i, j, output):

    if j == 0:

        if backtrack[i][j] == '*':

            output[0] = v[i] + output[0]
            output[1] = w[j] + output[1]

            return

        else:

            return

    if backtrack[i][j] == '-':

        return

    elif i == 0:

        output[0] = '-' + output[0]
        output[1] = w[j] + output[1]

        output_lcs(backtrack, v, w, i, j - 1, output)

    elif j == 0:

        output[0] = v[i] + output[0]
        output[1] = '-' + output[1]

        output_lcs(backtrack, v, w, i - 1, j, output)

    elif backtrack[i][j] == 'V':

        output[0] = v[i] + output[0]
        output[1] = '-' + output[1]

        output_lcs(backtrack, v, w, i - 1, j, output)

    elif backtrack[i][j] == '>':

        output[0] = '-' + output[0]
        output[1] = w[j] + output[1]

        output_lcs(backtrack, v, w, i, j - 1, output)

    else:

        output[0] = v[i] + output[0]
        output[1] = w[j] + output[1]

        output_lcs(backtrack, v, w, i - 1, j - 1, output)

    return


def overlap_alignment(string1, string2):

    lcs = lcs_backtrack(string2, string1)

    alignment_score = str(lcs[0])
    backtrack = lcs[1]

    output = ['', '']

    output_lcs(backtrack, string1, string2, lcs[2] - 1, lcs[3] - 1, output)

    return [alignment_score, output[0], output[1]]


f = open('dataset_248_7.txt', 'r')

lines = f.read().splitlines()

# print 'Overlap Alignment Problem: (expected output: 1 / HEAE / HEAG)'
# print "\n" . join(overlap_alignment('PAWHEAE', 'HEAGAWGHEE'))

# print 'Overlap Alignment Problem:'
# print "\n" . join(overlap_alignment(lines[0], lines[1]))

# Output:
# 13
# TACCTG-TCGTAACTTGTGC-CA-TTCT-AGG-CCCGTTTTCAC-TT-GCGCTT-ATGATCATGGTTCCGCTGATCTATATGGGCCGGGTAGGGCACTCC-CAGATGAAGGGGAGTAATG--GTAGCCGGATCCAAGTGACGCGC-CCTAGCGGCTCC-GGAGTTTGATAGACGTCGTG---C-TAT--GGAGCGTTGGAGCGACAAC--GCGCTCGTGCTCTGGAAGG-TCGC-TGCT-GATCCGT-AA
# TAC-TGGTCCTGACCCAC-CTCACTT-TGATGTCCCCTTTTCTCGTTTGCGCATCAAGATC-TGGC-CCGCA-A-CTAT-TGG-CCGTGAAAGGCACTCATCA-ATAAAGAC-AGTACTCACGCGGTCGGATCCAAATG-CGCGCACCGAGCGGC-CCAGGAGTT-GATAG-CGTCGAGTAACCTATTAGGA-C-TCG-AG-G-CAACTCGCGCTC-T-CTCAGGA-GGCTCGCCTGCTAG-TCCGTGAA

print 'Overlap Alignment Problem:'
print "\n" . join(overlap_alignment(lines[0], lines[1]))

# f.close()
