# -*- coding: utf-8 -*-


import sys


sys.setrecursionlimit(5000)


f = open('BLOSUM62.txt', 'r')

lines = f.read().splitlines()

gap_open_penalty = 11
gap_extension_penalty = 1

blosum_62 = {}

indexes = lines[0].split()

for x in indexes:

    blosum_62[x] = {}

    for y in indexes:

        blosum_62[x][y] = int(lines[indexes.index(x)+1][2:].split()[indexes.index(y)])

# for row in blosum_62.iteritems():
#     print row

f.close()


def lcs_backtrack(v, w):

    m = len(v)
    n = len(w)

    matrix_l = [[0 for row in range(0, m+1)] for col in range(0, n+1)]
    matrix_u = [[0 for row in range(0, m+1)] for col in range(0, n+1)]
    matrix_m = [[0 for row in range(0, m+1)] for col in range(0, n+1)]

    backtrack_l = [[' ' for row in range(0, m)] for col in range(0, n)]
    backtrack_u = [[' ' for row in range(0, m)] for col in range(0, n)]
    backtrack_m = [[' ' for row in range(0, m)] for col in range(0, n)]

    for i in range(1, n+1):

        matrix_l[i][0] = matrix_l[i-1][0] - gap_extension_penalty

    for j in range(1, m+1):

        matrix_u[0][j] = matrix_u[0][j-1] - gap_extension_penalty

    for i in range(1, n+1):

        for j in range(1, m+1):

            down_extension = matrix_l[i-1][j] - gap_extension_penalty
            down_open = matrix_m[i-1][j] - gap_open_penalty

            matrix_l[i][j] = max([down_extension, down_open])

            right_extension = matrix_u[i][j-1] - gap_extension_penalty
            right_open = matrix_m[i][j-1] - gap_open_penalty

            matrix_u[i][j] = max([right_extension, right_open])

            diagonal = matrix_m[i-1][j-1] + blosum_62[w[i-1]][v[j-1]]

            matrix_m[i][j] = max([matrix_l[i][j], matrix_u[i][j], diagonal])

            # create lower bactrack matrix

            if matrix_l[i][j] == down_extension:

                backtrack_l[i-1][j-1] = 'V'

            if matrix_l[i][j] == down_open:

                backtrack_l[i-1][j-1] = 'O'

            # create upper bactrack matrix

            if matrix_u[i][j] == right_extension:

                backtrack_u[i-1][j-1] = '>'

            if matrix_u[i][j] == right_open:

                backtrack_u[i-1][j-1] = 'O'

            # create middle bactrack matrix

            if matrix_m[i][j] == diagonal:

                backtrack_m[i-1][j-1] = '*'

            if matrix_m[i][j] == matrix_l[i][j]:

                backtrack_m[i-1][j-1] = 'V'

            if matrix_m[i][j] == matrix_u[i][j]:

                backtrack_m[i-1][j-1] = '>'

    alignment_score = matrix_m[i][j]

    # for row in matrix_l:
    #     print ' ' . join(['{:3}'.format(item) for item in row])
    # print ''
    # for row in matrix_u:
    #     print ' ' . join(['{:3}'.format(item) for item in row])
    # print ''
    # for row in matrix_m:
    #     print ' ' . join(['{:3}'.format(item) for item in row])
    # print ''

    # for row in backtrack_l:
    #     print '      ' + ' ' . join(['{:3}'.format(item) for item in row])
    # print ''
    # for row in backtrack_u:
    #     print '      ' + ' ' . join(['{:3}'.format(item) for item in row])
    # print ''
    # for row in backtrack_m:
    #     print '      ' + ' ' . join(['{:3}'.format(item) for item in row])
    # print ''

    return [alignment_score, backtrack_l, backtrack_u, backtrack_m]


def output_lcs(backtrack_l, backtrack_u, backtrack_m, level, v, w, i, j, output):

    if i == 0 and j == 0:

        if backtrack_m[i][j] == '*':

            output[0] = v[i] + output[0]
            output[1] = w[j] + output[1]

            return

        else:

            return

    if i == 0:

        output[0] = '-' + output[0]
        output[1] = w[j] + output[1]

        output_lcs(backtrack_l, backtrack_u, backtrack_m, level, v, w, i, j - 1, output)

    elif j == 0:

        output[0] = v[i] + output[0]
        output[1] = '-' + output[1]

        output_lcs(backtrack_l, backtrack_u, backtrack_m, level, v, w, i - 1, j, output)

    elif level == 'lower':

        output[0] = v[i] + output[0]
        output[1] = '-' + output[1]

        if backtrack_l[i][j] == 'V':

            output_lcs(backtrack_l, backtrack_u, backtrack_m, 'lower', v, w, i - 1, j, output)

        else:

            output_lcs(backtrack_l, backtrack_u, backtrack_m, 'middle', v, w, i - 1, j, output)

    elif level == 'upper':

        output[0] = '-' + output[0]
        output[1] = w[j] + output[1]

        if backtrack_u[i][j] == '>':

            output_lcs(backtrack_l, backtrack_u, backtrack_m, 'upper', v, w, i, j - 1, output)

        else:

            output_lcs(backtrack_l, backtrack_u, backtrack_m, 'middle', v, w, i, j - 1, output)

    elif backtrack_m[i][j] == 'V':

        output[0] = v[i] + output[0]
        output[1] = '-' + output[1]

        output_lcs(backtrack_l, backtrack_u, backtrack_m, 'lower', v, w, i - 1, j, output)

    elif backtrack_m[i][j] == '>':

        output[0] = '-' + output[0]
        output[1] = w[j] + output[1]

        output_lcs(backtrack_l, backtrack_u, backtrack_m, 'upper', v, w, i, j - 1, output)

    else:

        output[0] = v[i] + output[0]
        output[1] = w[j] + output[1]

        output_lcs(backtrack_l, backtrack_u, backtrack_m, level, v, w, i - 1, j - 1, output)

    return


def global_alignment(string1, string2):

    lcs = lcs_backtrack(string2, string1)

    aligment_score = str(lcs[0])
    backtrack_l = lcs[1]
    backtrack_u = lcs[2]
    backtrack_m = lcs[3]

    output = ['', '']

    output_lcs(backtrack_l, backtrack_u, backtrack_m, 'middle', string1, string2, len(string1) - 1, len(string2) - 1, output)

    return [aligment_score, output[0], output[1]]


f = open('dataset_249_8.txt', 'r')

lines = f.read().splitlines()

# print 'Global Alignment Problem: (expected output: 8 / PRT---EINS / PRTWPSEIN-)'
# print "\n" . join(global_alignment('PRTEINS', 'PRTWPSEIN'))

# print 'Global Alignment Problem: (expected output: 8 / AHRQPQ / AHE--D)'
# print "\n" . join(global_alignment('AHRQPQ', 'AHED'))

# print 'Global Alignment Problem:'
# print "\n" . join(global_alignment(lines[0], lines[1]))

# Output:
# 144
# YHFDVPDCWAHRYWVENPQAIAQME-------QICFNWFPSMMMK-------QPHVFKV---DHHMSCRWLPIRGKKCSSCCTRMRVRTVWE
# YHEDV----AHE------DAIAQMVNTFGFVWQICLNQFPSMMMKIYWIAVLSAHVADRKTWSKHMSCRWLPI----ISATCARMRVRTVWE

print 'Global Alignment Problem:'
print "\n" . join(global_alignment(lines[0], lines[1]))

f.close()
