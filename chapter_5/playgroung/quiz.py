# -*- coding: utf-8 -*-

import itertools


def generate_linear_peptides(length):

    patterns = []

    for i in itertools.product(['X', 'Z'], repeat=length):
        patterns.append(''.join(map(str, i)))

    return patterns

peptides_found = []

i = 5

while i < 16:

    for peptide in generate_linear_peptides(i):

        mass = 0

        for aminoacid in peptide:

            if aminoacid == 'X':

                mass += 2

            else:

                mass += 3

        if mass == 23:

            peptides_found.append(peptide)

    i += 1

    print len(peptides_found)

print peptides_found