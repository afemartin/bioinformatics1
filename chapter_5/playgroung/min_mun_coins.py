# -*- coding: utf-8 -*-


def min_num_coins(money, coins):

    if money > 0:

        num_coins = 9876543210

        for coin in coins:

            num_coins = min(num_coins, min_num_coins(money - coin, coins) + 1)

        return num_coins

    elif money == 0:

        return 0

    else:

        return 9876543210


print 'Minimum number of coins:'
print min_num_coins(10, [5, 4, 1])
