# -*- coding: utf-8 -*-


f = open('BLOSUM62.txt', 'r')

lines = f.read().splitlines()

indel_penalty = 5

blosum_62 = {}

indexes = lines[0].split()

for x in indexes:

    blosum_62[x] = {}

    for y in indexes:

        blosum_62[x][y] = int(lines[indexes.index(x)+1][2:].split()[indexes.index(y)])

f.close()


def find_middle_edge(v, w):

    n = len(v)
    m = len(w)

    middle_node_max = -987654321

    i = 1

    mid = int(m/2)

    curr_column = [0 for foo in range(0, n+1)]
    next_column = [0 for bar in range(0, n+1)]

    for j in range(1, len(curr_column)):

        curr_column[j] = curr_column[j-1] - indel_penalty

    while i <= mid + 1:

        next_column[0] = curr_column[0] - indel_penalty

        for j in range(1, len(curr_column)):

            down = next_column[j-1] - indel_penalty

            right = curr_column[j] - indel_penalty

            diagonal = curr_column[j-1] + blosum_62[w[i-1]][v[j-1]]

            next_column[j] = max([down, right, diagonal])

            # if mid <= j <= mid + 3 and mid - 1 <= i <= mid + 2:
            #
            #     print 'node ' + str((j, i)) + ' = ' + str(next_column[j]) + ' -> ' + str(mid)

            if i == mid:

                if middle_node_max < next_column[j]:

                    middle_node_max = next_column[j]

                    middle_node_coord = (j, i)

        if i <= mid:

            curr_column = list(next_column)

        i += 1

    # print middle_node_coord

    down = curr_column[middle_node_coord[0] + 1]

    right = next_column[middle_node_coord[0]]

    diagonal = next_column[middle_node_coord[0] + 1]

    # print 'down = ' + str(down) + '; right = ' + str(right) + '; diagonal = ' + str(diagonal)

    middle_edge_max = max([down, right, diagonal])

    if middle_edge_max == down:

        middle_edge_coord = (middle_node_coord[0], middle_node_coord[1] + 1)

    if middle_edge_max == right:

        middle_edge_coord = (middle_node_coord[0] + 1, middle_node_coord[1])

    if middle_edge_max == diagonal:

        middle_edge_coord = (middle_node_coord[0] + 1, middle_node_coord[1] + 1)

    return [middle_node_coord, middle_edge_coord]


f = open('dataset_250_12.txt', 'r')

lines = f.read().splitlines()

# print 'Middle edge: (expected output: (4, 3) (5, 4))'
# print " " . join(str(coord) for coord in find_middle_edge('PLEASANTLY', 'MEASNLY'))

# print 'Middle edge: (expected output: (512, 510) (513, 511))'
# print " " . join(str(coord) for coord in find_middle_edge(lines[0], lines[1]))

print 'Middle edge:'
print " " . join(str(coord) for coord in find_middle_edge(lines[0], lines[1]))

# f.close()
