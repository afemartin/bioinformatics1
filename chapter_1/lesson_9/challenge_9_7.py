# -*- coding: utf-8 -*-

import itertools
import time


# def generate_patterns(pattern_length):
#
#     patterns = []
#
#     for i in itertools.product(['A', 'C', 'G', 'T'], repeat=pattern_length):
#         patterns.append(''.join(map(str, i)))
#
#     return patterns


def generate_approximate_patterns(pattern, min_distance):

    charset = 'ACGT'

    for indices in itertools.combinations(range(len(pattern)), min_distance):
        for replacements in itertools.product(charset, repeat=min_distance):
            mutation = list(pattern)
            for index, replacement in zip(indices, replacements):
                mutation[index] = replacement
            yield "".join(mutation)


def hamming_distance(dataset1, dataset2):

    distance = 0
    i = 0

    datasets_length = len(dataset1)

    while i < datasets_length:

        nucleotide1 = dataset1[i:i+1]
        nucleotide2 = dataset2[i:i+1]

        if nucleotide1 != nucleotide2:
            distance += 1

        i += 1

    return distance


def approximate_pattern_count(dataset, pattern, min_distance):

    count = 0
    i = 0

    dataset_length = len(dataset)
    pattern_length = len(pattern)

    while i <= dataset_length - pattern_length:

        distance = hamming_distance(pattern, dataset[i:i+pattern_length])

        if distance <= min_distance:
            count += 1
        i += 1

    return count


def frequent_approximate_patterns(dataset, pattern_length, min_distance):

    generated_patterns = []
    patterns = []
    counts = []

    # generated_patterns = generate_patterns(pattern_length)

    dataset_length = len(dataset)

    i = 0

    while i <= dataset_length - pattern_length:
        pattern = dataset[i:i+pattern_length]
        generated_patterns += generate_approximate_patterns(pattern, min_distance)
        i += 1

    generated_patterns = list(set(generated_patterns))

    for pattern in generated_patterns:
        counts.append(approximate_pattern_count(dataset, pattern, min_distance))

    max_count = max(counts)

    i = 0

    while i < len(generated_patterns):
        if counts[i] == max_count:
            patterns.append(generated_patterns[i])
        i += 1

    return patterns


f = open('dataset_9_7.txt', 'r')

#example = f.read()

start = time.time()

#print 'Frequent approximate patterns: ' + ' ' . join(frequent_approximate_patterns(example, 8, 3))

#example = 'ACGTTGCATGTCGCATGATGCATGAGAGCT'

#print 'Frequent approximate patterns: ' + ' ' . join(frequent_approximate_patterns(example, 4, 1))

# GATG ATGC ATGT
# GATG ATGC ATGT

#example = 'CACAGTAGGCGCCGGCACACACAGCCCCGGGCCCCGGGCCGCCCCGGGCCGGCGGCCGCCGGCGCCGGCACACCGGCACAGCCGTACCGGCACAGTAGTACCGGCCGGCCGGCACACCGGCACACCGGGTACACACCGGGGCGCACACACAGGCGGGCGCCGGGCCCCGGGCCGTACCGGGCCGCCGGCGGCCCACAGGCGCCGGCACAGTACCGGCACACACAGTAGCCCACACACAGGCGGGCGGTAGCCGGCGCACACACACACAGTAGGCGCACAGCCGCCCACACACACCGGCCGGCCGGCACAGGCGGGCGGGCGCACACACACCGGCACAGTAGTAGGCGGCCGGCGCACAGCC'

#print 'Frequent approximate patterns: ' + ' ' . join(frequent_approximate_patterns(example, 10, 2))

# GCACACAGAC GCGCACACAC
# GCACACAGAC GCGCACACAC

print '3-neighborhood: ' + ' ' . join(list(set(generate_approximate_patterns('TAGC', 3))))
print len(list(set(generate_approximate_patterns('TAGC', 3))))

end = time.time()

print 'Execution time: ' + str(end - start) + ' segs'

# f.close()
