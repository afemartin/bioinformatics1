# -*- coding: utf-8 -*-


def hamming_distance(dataset1, dataset2):

    distance = 0
    i = 0

    datasets_length = len(dataset1)

    while i < datasets_length:

        nucleotide1 = dataset1[i:i+1]
        nucleotide2 = dataset2[i:i+1]

        if nucleotide1 != nucleotide2:
            distance += 1

        i += 1

    return distance


def approximate_pattern_matching(dataset, pattern, min_distance):

    count = 0
    i = 0

    dataset_length = len(dataset)
    pattern_length = len(pattern)

    while i <= dataset_length - pattern_length:

        distance = hamming_distance(pattern, dataset[i:i+pattern_length])

        if distance <= min_distance:
            count += 1

        i += 1

    return count

f = open('dataset_9_6.txt', 'r')

example = f.read()

print 'Approximate patterns found: ' + str(approximate_pattern_matching(example, 'GTTCGCG', 3))

f.close()
