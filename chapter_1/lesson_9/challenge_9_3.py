# -*- coding: utf-8 -*-


def hamming_distance(dataset1, dataset2):

    distance = 0
    i = 0

    datasets_length = len(dataset1)

    while i < datasets_length:

        nucleotide1 = dataset1[i:i+1]
        nucleotide2 = dataset2[i:i+1]

        if nucleotide1 != nucleotide2:
            distance += 1

        i += 1

    return distance


f = open('dataset_9_3.txt', 'r')

example1 = f.readline()
example2 = f.readline()

print 'Hamming distance: ' + str(hamming_distance(example1, example2))
print 'Hamming distance: ' + str(hamming_distance('CTTGAAGTGGACCTCTAGTTCCTCTACAAAGAACAGGTTGACCTGTCGCGAAG', 'ATGCCTTACCTAGATGCAATGACGGACGTATTCCTTTTGCCTCAACGGCTCCT'))

f.close()
