# -*- coding: utf-8 -*-

import itertools
import time


def reverse_pattern(pattern):

    #start = time.time()

    reverse_complement = ''

    for c in reversed(pattern):
        if c == 'A':
            reverse_complement += 'T'
        elif c == 'C':
            reverse_complement += 'G'
        elif c == 'G':
            reverse_complement += 'C'
        elif c == 'T':
            reverse_complement += 'A'

    #end = time.time()

    #print 'Execution time: reverse_pattern(' + pattern + ') [' + str((end - start) * 1000) + ' ms]'

    return reverse_complement


def generate_approximate_patterns(pattern, min_distance):

    #start = time.time()

    charset = 'ACGT'

    for indices in itertools.combinations(range(len(pattern)), min_distance):
        for replacements in itertools.product(charset, repeat=min_distance):
            mutation = list(pattern)
            for index, replacement in zip(indices, replacements):
                mutation[index] = replacement
            yield "".join(mutation)

    #end = time.time()

    #print 'Execution time: generate_approximate_patterns(' + pattern + ', ' + str(min_distance) + ') [' + str((end - start) * 1000) + ' ms]'


def hamming_distance(dataset1, dataset2):

    #start = time.time()

    distance = 0
    i = 0

    datasets_length = len(dataset1)

    while i < datasets_length:

        nucleotide1 = dataset1[i:i+1]
        nucleotide2 = dataset2[i:i+1]

        if nucleotide1 != nucleotide2:
            distance += 1

        i += 1

    #end = time.time()

    #print 'Execution time: hamming_distance(' + dataset1 + ', ' + dataset2 + ') [' + str((end - start) * 1000) + ' ms]'

    return distance


# def approximate_pattern_count(dataset, pattern, min_distance):
#
#     start = time.time()
#
#     count = 0
#     i = 0
#
#     dataset_length = len(dataset)
#     pattern_length = len(pattern)
#
#     while i <= dataset_length - pattern_length:
#
#         distance = hamming_distance(pattern, dataset[i:i+pattern_length])
#
#         if distance <= min_distance:
#             count += 1
#         i += 1
#
#     end = time.time()
#
#     print 'Execution time: approximate_pattern_count(<dataset>, ' + pattern + ', ' + str(min_distance) + ') [' + str((end - start) * 1000) + ' ms]'
#
#     return count


def approximate_pattern_count(dataset, pattern, min_distance):

    #start = time.time()

    count = 0
    i = 0

    dataset_length = len(dataset)
    pattern_length = len(pattern)

    while i <= dataset_length - pattern_length:
        distance = hamming_distance(pattern, dataset[i:i+pattern_length])
        if distance <= min_distance:
            count += 1
        i += 1

    #end = time.time()

    #print 'Execution time: approximate_pattern_count(<dataset>, ' + pattern + ', ' + str(min_distance) + ') [' + str((end - start) * 1000) + ' ms]'

    return count


def frequent_approximate_and_reverse_patterns(dataset, pattern_length, min_distance):

    generated_patterns = []
    patterns = []
    counts = []

    dataset_length = len(dataset)

    i = 0

    while i <= dataset_length - pattern_length:

        pattern = dataset[i:i+pattern_length]

        reversed_pattern = reverse_pattern(pattern)

        generated_patterns += generate_approximate_patterns(pattern, min_distance)

        if pattern != reversed_pattern:
            generated_patterns += generate_approximate_patterns(reverse_pattern(pattern), min_distance)

        i += 1

    generated_patterns = list(set(generated_patterns))

    for pattern in generated_patterns:

        reversed_pattern = reverse_pattern(pattern)

        aproximate_and_reverse_pattern_count = approximate_pattern_count(dataset, pattern, min_distance)

        if pattern != reversed_pattern:
            aproximate_and_reverse_pattern_count += approximate_pattern_count(dataset, reverse_pattern(pattern), min_distance)

        counts.append(aproximate_and_reverse_pattern_count)

    max_count = max(counts)

    i = 0

    generated_patterns_length = len(generated_patterns)

    while i < generated_patterns_length:
        if counts[i] == max_count:
            patterns.append(generated_patterns[i])
        i += 1

    return patterns


f = open('dataset_9_8.txt', 'r')

#example = f.read()

start = time.time()

#print 'Frequent approximate patterns: ' + ' ' . join(frequent_approximate_and_reverse_patterns(example, 9, 2))

#example = 'ACGTTGCATGTCGCATGATGCATGAGAGCT'

#print 'Frequent approximate patterns: ' + ' ' . join(frequent_approximate_and_reverse_patterns(example, 4, 1))

# ATGT ACAT (OK)
# ATGT ACAT (reverse added to the list of patterns to count)

#example = 'CTTGCCGGCGCCGATTATACGATCGCGGCCGCTTGCCTTCTTTATAATGCATCGGCGCCGCGATCTTGCTATATACGTACGCTTCGCTTGCATCTTGCGCGCATTACGTACTTATCGATTACTTATCTTCGATGCCGGCCGGCATATGCCGCTTTAGCATCGATCGATCGTACTTTACGCGTATAGCCGCTTCGCTTGCCGTACGCGATGCTAGCATATGCTAGCGCTAATTACTTAT'

#print 'Frequent approximate patterns: ' + ' ' . join(frequent_approximate_and_reverse_patterns(example, 9, 3))

# AGCGCCGCT AGCGGCGCT (OK)
# AGCGCCGCT AGCGGCGCT (reverse added to the list of patterns to count)

print

end = time.time()

print 'Execution time: ' + str(end - start) + ' segs'

# f.close()
