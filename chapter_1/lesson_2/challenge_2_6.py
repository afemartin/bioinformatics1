# -*- coding: utf-8 -*-


def pattern_count(dataset, pattern):

    count = 0
    i = 0

    dataset_length = len(dataset)
    pattern_length = len(pattern)

    while i <= dataset_length - pattern_length:
        if dataset[i:i+pattern_length] == pattern:
            count += 1
        i += 1

    return count


f = open('dataset_2_6.txt', 'r')

example = f.read()

print pattern_count(example, 'ACTTCAGAC')

f.close()
