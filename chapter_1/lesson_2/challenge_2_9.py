# -*- coding: utf-8 -*-


def pattern_count(dataset, pattern):

    count = 0
    i = 0

    dataset_length = len(dataset)
    pattern_length = len(pattern)

    while i <= dataset_length - pattern_length:
        if dataset[i:i+pattern_length] == pattern:
            count += 1
        i += 1

    return count


def frequent_patterns(dataset, k):

    patterns = []
    counts = []

    i = 0

    dataset_length = len(dataset)

    while i <= dataset_length - k:
        pattern = dataset[i:i+k]
        counts.append(pattern_count(dataset, pattern))
        i += 1

    max_count = max(counts)

    i = 0

    while i <= dataset_length - k:
        if counts[i] == max_count:
            patterns.append(dataset[i:i+k])
        i += 1

    return list(set(patterns))


f = open('dataset_2_9.txt', 'r')

example = f.read()

print '11-mers: ' + ' ' . join(frequent_patterns(example, 11))

f.close()
