# -*- coding: utf-8 -*-


def pattern_count(dataset, pattern):

    count = 0
    i = 0

    dataset_length = len(dataset)
    pattern_length = len(pattern)

    while i <= dataset_length - pattern_length:
        if dataset[i:i+pattern_length] == pattern:
            count += 1
        i += 1

    return count


f = open('Vibrio_cholerae.txt', 'r')

example = f.read()

print 'Input'
print example
print 'Output'
print pattern_count(example, 'ACTAT')

f.close()
