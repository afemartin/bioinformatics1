# -*- coding: utf-8 -*-

import time


def pattern_count(dataset, pattern):

    count = 0
    i = 0

    dataset_length = len(dataset)
    pattern_length = len(pattern)

    while i <= dataset_length - pattern_length:
        if dataset[i:i+pattern_length] == pattern:
            count += 1
        i += 1

    return count


def pattern_clumps(dataset, k, l, t):

    patterns = []

    i = 0

    dataset_length = len(dataset)

    while i <= dataset_length - k:
        pattern = dataset[i:i+k]
        if pattern_count(dataset[i:i+l], pattern) >= t:
            patterns.append(pattern)
        i += 1

    return list(set(patterns))


f = open('E-coli.txt', 'r')

example = f.read()

start = time.time()

print 'Patterns forming (500,3)-lumps: ' + str(len(pattern_clumps(example, 9, 500, 3)))

end = time.time()

print 'Execution time: ' + str(end - start) + 'segs'

f.close()
