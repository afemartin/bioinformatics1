# -*- coding: utf-8 -*-

import time


def skew(dataset):

    skew_list = []

    skew_value = 0

    skew_list.append(skew_value)

    for nucleotide in dataset:
        if nucleotide == 'C':
            skew_value -= 1
        elif nucleotide == 'G':
            skew_value += 1
        skew_list.append(skew_value)

    return skew_list


start = time.time()

print 'Skew: ' + ' ' . join(str(n) for n in skew('TAAAGACTGCCGAGAGGCCAACACGAGTGCTAGAACGAGGGGCGTAAACGCGGGTCCGAT'))
print 'Skew: ' + ' ' . join(str(n) for n in skew('CATTCCAGTACTTCATGATGGCGTGAAGA'))

end = time.time()

print 'Execution time: ' + str((end - start) * 1000) + ' milisegs'
