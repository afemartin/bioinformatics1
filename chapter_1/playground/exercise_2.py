# -*- coding: utf-8 -*-


def pattern_positions(dataset, pattern):

    positions = []

    i = 0

    dataset_length = len(dataset)
    pattern_length = len(pattern)

    while i <= dataset_length - pattern_length:
        if dataset[i:i+pattern_length] == pattern:
            positions.append(i)
        i += 1

    return positions


f = open('Vibrio_cholerae.txt', 'r')

example = f.read()

print 'Positions: ' + ' ' . join(str(n) for n in pattern_positions(example, 'CTTGATCAT'))
print 'Positions: ' + ' ' . join(str(n) for n in pattern_positions(example, 'ATGATCAAG'))

f.close()
