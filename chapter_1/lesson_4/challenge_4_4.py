# -*- coding: utf-8 -*-


def pattern_count(dataset, pattern):

    count = 0
    i = 0

    dataset_length = len(dataset)
    pattern_length = len(pattern)

    while i <= dataset_length - pattern_length:
        if dataset[i:i+pattern_length] == pattern:
            count += 1
        i += 1

    return count


def pattern_clumps(dataset, k, l, t):

    patterns = []

    i = 0

    dataset_length = len(dataset)

    while i <= dataset_length - k:
        pattern = dataset[i:i+k]
        if pattern_count(dataset[i:i+l], pattern) >= t:
            patterns.append(pattern)
        i += 1

    return list(set(patterns))


f = open('dataset_4_4.txt', 'r')

example = f.read()

print 'Patterns forming (494,17)-lumps: ' + ' ' . join(pattern_clumps(example, 9, 494, 17))
print 'Patterns forming (30,3)-lumps: ' + ' ' . join(pattern_clumps('GCACAAGGCCGACAATAGGACGTAGCCTTGAAGACGACGTAGCGTGGTCGCATAAGTACAGTAGATAGTACCTCCCCCGCGCATCCTATTATTAAGTTAATT', 4, 30, 3))

f.close()
