# -*- coding: utf-8 -*-


def reverse_pattern(pattern):

    reverse_complement = ''

    for c in reversed(pattern):
        if c == 'A':
            reverse_complement += 'T'
        elif c == 'C':
            reverse_complement += 'G'
        elif c == 'G':
            reverse_complement += 'C'
        elif c == 'T':
            reverse_complement += 'A'

    return reverse_complement


f = open('dataset_3_2.txt', 'r')

example = f.read()

print reverse_pattern(example)
print reverse_pattern('GCTAGCT')

f.close()
