# -*- coding: utf-8 -*-


def minimun_skew(dataset):

    min_positions = [0]

    skew_current_value = 0
    skew_minimum_value = 0
    i = 0

    dataset_length = len(dataset)

    while i < dataset_length:

        nucleotide = dataset[i:i+1]

        if nucleotide == 'C':
            skew_current_value -= 1
        elif nucleotide == 'G':
            skew_current_value += 1

        i += 1

        if skew_current_value < skew_minimum_value:
            min_positions[:] = []
            min_positions.append(i)
            skew_minimum_value = skew_current_value
        elif skew_current_value == skew_minimum_value:
            min_positions.append(i)

    return min_positions


f = open('dataset_7_6.txt', 'r')

example = f.read()

print 'Positions where skew is minimum: ' + ' ' . join(str(n) for n in minimun_skew(example))
print 'Positions where skew is minimum: ' + ' ' . join(str(n) for n in minimun_skew('GATACACTTCCCAGTAGGTACTG'))

f.close()
